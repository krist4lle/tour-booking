<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->id();

            // Data
            $table->string('name');
            $table->string('description', 510)->nullable();
            $table->float('price', 8, 2, true)->nullable();

            // Relations
            $table->foreignId('training_type_id')->constrained('training_types')->restrictOnDelete();
            $table->foreignId('sport_id')->constrained('sports')->restrictOnDelete();
            $table->foreignId('trainer_id')->constrained('users')->cascadeOnDelete();

            // Schedule
            $table->dateTime('start_datetime');
            $table->unsignedInteger('duration');

            // Location (training can be online or not)
            $table->string('location')->nullable();
            $table->foreignId('online_platform_id')->nullable()->constrained('online_platforms')->nullOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trainings');
    }
};
