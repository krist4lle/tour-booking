<?php

namespace Database\Seeders;

use App\Services\RolePermission\RolePermissionService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /** @var RolePermissionService $service */
        $service = app(RolePermissionService::class);

        foreach ($service->getDomains() as $domain) {
            foreach ($service->getActions() as $action) {
                Permission::updateOrCreate(
                    ['name' => "{$action} {$domain->getClass()}"],
                    ['guard_name' => 'web']
                );
            }
        }
    }
}
