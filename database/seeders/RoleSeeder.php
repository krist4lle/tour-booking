<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    private array $roles = [
        [
            'id' => 1,
            'name' => 'admin',
        ],
        [
            'id' => 2,
            'name' => 'trainer',
        ],
        [
            'id' => 3,
            'name' => 'student',
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->roles as $role) {
            Role::updateOrCreate($role);
        }
    }
}
