<?php

namespace Database\Seeders;

use App\Models\OnlinePlatform;
use Illuminate\Database\Seeder;

class OnlinePlatformSeeder extends Seeder
{
    private array $platforms = [
        [
            'id' => 1,
            'name' => 'Instagram',
            'slug' => 'instagram',
        ],
        [
            'id' => 2,
            'name' => 'Telegram',
            'slug' => 'telegram',
        ],
        [
            'id' => 3,
            'name' => 'WhatsApp',
            'slug' => 'whats_app',
        ],
        [
            'id' => 4,
            'name' => 'Viber',
            'slug' => 'viber',
        ],
        [
            'id' => 5,
            'name' => 'Zoom',
            'slug' => 'zoom',
        ],
        [
            'id' => 6,
            'name' => 'GoogleMeet',
            'slug' => 'google_meet',
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->platforms as $platform) {
            OnlinePlatform::updateOrCreate(
                [
                    'id' => $platform['id'],
                ],
                $platform
            );
        }
    }
}
