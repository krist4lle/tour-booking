<?php

namespace Database\Seeders;

use App\Models\Sport;
use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    private array $sports = [
        [
            'name' => 'Step',
        ],
        [
            'name' => 'Pilates',
        ],
        [
            'name' => 'Pump',
        ],
        [
            'name' => 'Fitness',
        ],
        [
            'name' => 'Zumba',
        ],
        [
            'name' => 'Yoga',
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->sports as $sport) {
            Sport::updateOrCreate(
                ['name' => $sport['name']]
            );
        }
    }
}
