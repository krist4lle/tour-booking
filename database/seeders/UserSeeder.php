<?php

namespace Database\Seeders;

use App\Exceptions\FactoryException;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\User;
use App\Models\UserProfile;
use App\Repositories\OnlinePlatform\OnlinePlatformRepository;
use App\Repositories\Sport\SportRepository;
use App\Repositories\User\UserRepository;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class UserSeeder extends Seeder
{
    private \Faker\Generator $faker;

    private array $allSports;

    private UserRepository $userRepository;

    private array $allOnlinePlatforms;

    public function __construct()
    {
        $this->faker = Faker::create();
        $this->userRepository = new UserRepository();

        $sportRepository = new SportRepository();
        $this->allSports = $sportRepository->getAll()->pluck('id')->toArray();

        $onlinePlatformRepository = new OnlinePlatformRepository();
        $this->allOnlinePlatforms = $onlinePlatformRepository->getAll()->pluck('id')->toArray();
    }

    private array $genders = [
        UserProfile::MALE,
        UserProfile::FEMALE,
    ];

    /**
     * @throws FactoryException
     */
    public function run(): void
    {
        $counter = $this->userRepository->getAll()->count();

        if ($counter < 50) {
            for ($i = 0; $i < 50; $i++) {
                $user = $this->createUser();

                $this->createUserProfile($user->id);
                $this->attachSports($user);
                $this->attachOnlinePlatforms($user);

                $counter++;

                if ($counter === 50) {
                    break;
                }
            }
        }
    }

    /**
     * @throws FactoryException
     */
    private function createUser(): User
    {
        return UserFactory::make([
            'email' => $this->faker->email(),
            'password' => 'password',
            'role_id' => rand(2, 3),
        ]);
    }

    /**
     * @throws FactoryException
     */
    private function createUserProfile(int $userId): void
    {
        $gender = $this->genders[rand(0, 1)];

        UserProfileFactory::make([
            'user_id' => $userId,
            'first_name' => $this->faker->firstName($gender === UserProfile::MALE ? 'male' : 'female'),
            'last_name' => $this->faker->lastName(),
            'gender' => $gender,
            'bio' => $this->faker->text(500),
            'country' => $this->faker->country(),
            'date_of_birth' => $this->faker->dateTimeBetween('-50 years', '-20 years')->format('d.m.Y'),
        ]);
    }

    private function attachSports(User $user): void
    {
        if ($user->isTrainer()) {
            $userSports = rand(1, count($this->allSports));
            $user->sports()->sync(Arr::random($this->allSports, $userSports));
        }
    }

    private function attachOnlinePlatforms(User $user): void
    {
        $userPlatforms = rand(1, count($this->allOnlinePlatforms));
        $user->onlinePlatforms()->syncWithPivotValues(
            Arr::random($this->allOnlinePlatforms, $userPlatforms),
            ['account' => '@platform_account']
        );
    }
}
