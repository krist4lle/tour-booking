<?php

namespace Database\Seeders;

use App\Factories\Training\TrainingFactory;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\Training;
use App\Models\TrainingType;
use App\Models\User;
use App\Repositories\OnlinePlatform\OnlinePlatformRepository;
use App\Repositories\Sport\SportRepository;
use App\Repositories\Training\TrainingRepository;
use App\Repositories\TrainingType\TrainingTypeRepository;
use App\Repositories\User\UserRepository;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class TrainingSeeder extends Seeder
{
    private \Faker\Generator $faker;

    private \Illuminate\Support\Collection $trainers;

    private \Illuminate\Support\Collection $students;

    private \Illuminate\Support\Collection $sports;

    private \Illuminate\Support\Collection $trainingTypes;

    private \Illuminate\Support\Collection $onlinePlatforms;

    public function __construct()
    {
        $this->faker = Faker::create();

        $userRepo = new UserRepository(User::class);
        $this->trainers = $userRepo->getTrainers();
        $userRepo->refreshQuery();
        $this->students = $userRepo->getStudents();

        $sportRepo = new SportRepository(Sport::class);
        $this->sports = $sportRepo->getAll();

        $trainingTypeRepo = new TrainingTypeRepository(TrainingType::class);
        $this->trainingTypes = $trainingTypeRepo->getAll();

        $onlinePlatformRepo = new OnlinePlatformRepository(OnlinePlatform::class);
        $this->onlinePlatforms = $onlinePlatformRepo->getAll();
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $trainingRepo = new TrainingRepository(Training::class);
        $counter = $trainingRepo->getAll()->count();

        if ($counter < 100) {
            for ($i = 0; $i < 100; $i++) {
                $online = rand(0, 1);

                $training = TrainingFactory::make([
                    'name' => $this->faker->jobTitle(),
                    'description' => $this->faker->text(400),
                    'price' => $this->faker->randomFloat(2, 0, 30),
                    'online' => $online,
                    'start_datetime' => $this->faker->dateTimeBetween('today', '+1 month'),
                    'duration' => rand(30, 90),
                    'location' => $online === 0 ? $this->faker->streetAddress() : null,
                    'online_platform_id' => $online === 1 ? $this->onlinePlatforms->random()->id : null,
                    'training_type_id' => $this->trainingTypes->random()->id,
                    'sport_id' => $this->sports->random()->id,
                    'trainer_id' => $this->trainers->random()->id,
                ]);

                $students = $training->isPersonalType() ? [$this->students->random()->id] : $this->students->random(rand(1, 5));
                $training->students()->sync($students);

                $counter++;

                if ($counter === 100) {
                    break;
                }
            }
        }
    }
}
