<?php

namespace Database\Seeders;

use App\Models\TrainingType;
use Illuminate\Database\Seeder;

class TrainingTypeSeeder extends Seeder
{
    private array $types = [
        [
            'id' => 1,
            'name' => 'Group',
            'slug' => 'group',
        ],
        [
            'id' => 2,
            'name' => 'Personal',
            'slug' => 'personal',
        ],
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->types as $type) {
            TrainingType::updateOrCreate(
                [
                    'id' => $type['id'],
                ],
                $type
            );
        }
    }
}
