import './bootstrap';
import '../css/app.css';

import { createApp } from 'vue';
import {store} from "@/store/store";
import {router} from "@/router/router";
import {i18n} from "@/lang/lang";
import Notifications from '@kyvg/vue3-notification'
import App from "@/App.vue";

const app = createApp(App)
    .use(store)
    .use(router)
    .use(i18n)
    .use(Notifications)
    .mount('#app');
