const getters = {
    currentUser(state) {
        return state.currentUser;
    },
    selectOptions(state) {
        return state.selectOptions;
    },
    users(state) {
        return state.users;
    },
    user(state) {
        return state.user;
    },
    usersMeta(state) {
        return state.usersMeta;
    }
}

export default getters;
