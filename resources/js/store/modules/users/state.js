import {user} from "@/store/models/user";
import meta from "@/store/models/meta";

const state = {
    currentUser: user,
    user: user,
    selectOptions: [],
    users: [],
    usersMeta: meta
}

export default state;
