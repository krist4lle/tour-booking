import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/users');

const actions = {
    async fetchCurrentUser() {
        const response = await http.get(uris.current());

        store.commit('users/setCurrentUser', response.data.data);
    },
    async fetchSelectOptions({ commit }, payload) {
        const response = await http.get(uris.select(), {params: payload});

        store.commit('users/setSelectOptions', response.data.data);
    },
    async fetchUsers({ commit }, payload) {
        const response = await http.get(uris.index(), {params: payload});

        store.commit('users/setUsers', response.data.data);
        store.commit('users/setUsersMeta', response.data.meta);
    },
    async fetchUser({ commit }, userId) {
        const response = await http.get(uris.show(userId));

        store.commit('users/setUser', response.data.data);
    },
    async storeUser({ commit }, payload) {
        return await http.post(uris.index(), payload);
    },
    async updateUser({ commit }, payload) {
        return await http.put(uris.show(payload.id), payload);
    },
    async deleteUser({ commit }, payload) {
        return await http.delete(uris.show(payload.id));
    },
    async updateUserPassword({ commit }, payload) {
        return await http.post(uris.index() + '/update-password', payload);
    },
    async setLocale({ commit }, locale) {
        await http.get(uris.index() + '/set-locale', {params: {locale: locale}});
    }
}

export default actions;
