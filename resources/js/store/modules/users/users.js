import state from "@/store/modules/users/state";
import mutations from "@/store/modules/users/mutations";
import actions from "@/store/modules/users/actions";
import getters from "@/store/modules/users/getters";

const UsersModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default UsersModule;
