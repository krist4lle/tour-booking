const mutations = {
    setCurrentUser(state, user) {
        state.currentUser = user;
    },
    setSelectOptions(state, options) {
        state.selectOptions = options;
    },
    setUsers(state, users) {
        state.users = users;
    },
    setUser(state, user) {
        state.user = user;
    },
    setUsersMeta(state, meta) {
        state.usersMeta = meta;
    },
}

export default mutations;
