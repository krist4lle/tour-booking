import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/roles');

const actions = {
    async fetchRoles() {
        const response = await http.get(uris.index());

        store.commit('roles/setRoles', response.data.data);
    },
    async fetchRole({ commit }, id) {
        const response = await http.get(uris.show(id));

        store.commit('roles/setRole', response.data.data);
    },
    async syncPermissions({ commit }, payload) {
        return await http.post(uris.sync(), payload);
    }
}

export default actions;
