const getters = {
    roles(state) {
        return state.roles;
    },
    role(state) {
      return state.role;
    },
    selectOptions(state) {
        return state.selectOptions;
    }
}

export default getters;
