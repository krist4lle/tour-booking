import {role} from "@/store/models/role";

const state = {
    roles: [],
    role: role,
    selectOptions: []
}

export default state;
