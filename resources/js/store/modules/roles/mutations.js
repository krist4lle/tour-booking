const mutations = {
    setRoles(state, roles) {
        state.roles = roles;
    },
    setRole(state, role) {
        state.role = role;
    },
}

export default mutations;
