import state from "@/store/modules/roles/state";
import mutations from "@/store/modules/roles/mutations";
import actions from "@/store/modules/roles/actions";
import getters from "@/store/modules/roles/getters";

const RolesModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default RolesModule;
