const mutations = {
    setSports(state, sports) {
        state.sports = sports;
    },
    setSportsMeta(state, meta) {
        state.sportsMeta = meta;
    },
    setSelectOptions(state, options) {
        state.selectOptions = options;
    }
}

export default mutations;
