import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/sports');

const actions = {
    async fetchSports({ commit }, payload) {
      const response = await http.get(uris.index(), {params: payload});

      store.commit('sports/setSports', response.data.data);
      store.commit('sports/setSportsMeta', response.data.meta);
    },
    async fetchSelectOptions({ commit }, payload) {
        const response = await http.get(uris.select(), {params: payload});

        store.commit('sports/setSelectOptions', response.data.data);
    },
    async storeSport({ commit }, payload) {
        return http.post(uris.index(), payload);
    },
    async updateSport({ commit }, payload) {
        return http.put(uris.show(payload.id), payload);
    },
    async deleteSport({ commit }, payload) {
        return http.delete(uris.show(payload.id));
    },
    async attachSport({ commit }, payload) {
        return await http.post(uris.attach(), payload);
    },
    async detachSport({ commit }, payload) {
        return await http.post(uris.detach(), payload);
    },
}

export default actions;
