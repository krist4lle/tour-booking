import meta from "@/store/models/meta";

const state = {
    sports: [],
    sportsMeta: meta,
    selectOptions: []
}

export default state;
