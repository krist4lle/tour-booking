import state from "@/store/modules/sports/state";
import mutations from "@/store/modules/sports/mutations";
import actions from "@/store/modules/sports/actions";
import getters from "@/store/modules/sports/getters";

const SportsModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default SportsModule;
