const getters = {
    sports(state) {
        return state.sports;
    },
    sportsMeta(state) {
        return state.sportsMeta;
    },
    selectOptions(state) {
        return state.selectOptions;
    }
}

export default getters;
