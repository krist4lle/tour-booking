import meta from "@/store/models/meta";

const state = {
    permissions: [],
    permissionsMeta: meta,
    domainSelectOptions: []
}

export default state;
