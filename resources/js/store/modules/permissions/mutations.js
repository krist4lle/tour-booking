const mutations = {
    setPermissions(state, permissions) {
        state.permissions = permissions;
    },
    setPermissionsMeta(state, meta) {
        state.permissionsMeta = meta;
    },
    setDomainSelectOptions(state, options) {
        state.domainSelectOptions = options;
    }
}

export default mutations;
