import state from "@/store/modules/permissions/state";
import mutations from "@/store/modules/permissions/mutations";
import actions from "@/store/modules/permissions/actions";
import getters from "@/store/modules/permissions/getters";

const PermissionsModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default PermissionsModule;
