const getters = {
    permissions(state) {
        return state.permissions;
    },
    permissionsMeta(state) {
        return state.permissionsMeta;
    },
    domainSelectOptions(state) {
        return state.domainSelectOptions;
    }
}

export default getters;
