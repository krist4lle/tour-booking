import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/permissions');

const actions = {
    async fetchPermissions({ commit }, payload) {
        const response = await http.get(uris.index(), {params: payload});

        store.commit('permissions/setPermissions', response.data.data);
        store.commit('permissions/setPermissionsMeta', response.data.meta);
    },
    async fetchDomainsSelect() {
        const response = await http.get(uris.select() + '-domains');

        store.commit('permissions/setDomainSelectOptions', response.data.data);
    },
    async storePermission({ commit }, payload) {
        return await http.post(uris.index(), payload);
    },
    async updatePermission({ commit }, payload) {
        return await http.put(uris.show(payload.id), payload);
    },
    async deletePermission({ commit }, payload) {
        return await http.delete(uris.show(payload.id));
    }
}

export default actions;
