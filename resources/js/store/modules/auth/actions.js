import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";

const uris = getUris('/auth');

const actions = {
    async registerUser({ commit }, payload) {
        return await http.post(uris.index() + '/register', payload);
    },
    async loginUser({ commit }, payload) {
        return await http.post(uris.index() + '/login', payload);
    },
    async logoutUser() {
        return await http.post(uris.index() + '/logout');
    }
}

export default actions;
