import state from "@/store/modules/trainings/state";
import mutations from "@/store/modules/trainings/mutations";
import actions from "@/store/modules/trainings/actions";
import getters from "@/store/modules/trainings/getters";

const TrainingsModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default TrainingsModule;
