const mutations = {
    setTrainings(state, trainings) {
        state.trainings = trainings;
    },
    setTraining(state, training) {
        state.training = training;
    },
    setTrainingsMeta(state, meta) {
        state.trainingsMeta = meta;
    },
}

export default mutations;
