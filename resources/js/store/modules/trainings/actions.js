import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/trainings');

const actions = {
    async fetchTrainings({ commit }, payload) {
        const response = await http.get(uris.index(), {params: payload});

        store.commit('trainings/setTrainings', response.data.data);
        store.commit('trainings/setTrainingsMeta', response.data.meta);
    },
    async fetchTraining({ commit }, trainingId) {
        const response = await http.get(uris.show(trainingId));

        store.commit('trainings/setTraining', response.data.data);
    },
    async storeTraining({ commit }, payload) {
        return await http.post(uris.index(), payload);
    },
    async updateTraining({ commit }, payload) {
        return await http.put(uris.show(payload.id), payload);
    },
    async deleteTraining({ commit }, payload) {
        return await http.delete(uris.show(payload.id));
    },
    async attachUserToTraining({ commit }, payload) {
        return await http.post(uris.attach(), payload);
    },
    async detachUserFromTraining({ commit }, payload) {
        return await http.post(uris.detach(), payload);
    },
}

export default actions;
