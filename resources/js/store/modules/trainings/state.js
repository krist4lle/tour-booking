import meta from "@/store/models/meta";
import training from "@/store/models/training";

const state = {
    training: training,
    trainings: [],
    usersMeta: meta
}

export default state;
