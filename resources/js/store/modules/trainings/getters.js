const getters = {
    trainings(state) {
        return state.trainings;
    },
    training(state) {
        return state.training;
    },
    trainingsMeta(state) {
        return state.trainingsMeta;
    }
}

export default getters;
