import searchQuery from "@/store/models/search-query";

const state = {
    searchQuery: {...searchQuery}
}

export default state;
