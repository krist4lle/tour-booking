const mutations = {
    setSearchQuery(state, searchQuery) {
        state.searchQuery = searchQuery;
    },
    setFilter(state, payload) {
        if (payload.value == 0) {
            payload.value = null;
        }

        state.searchQuery[payload.field] = payload.value;
    }
}

export default mutations;
