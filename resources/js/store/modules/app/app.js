import state from "@/store/modules/app/state";
import mutations from "@/store/modules/app/mutations";
import actions from "@/store/modules/app/actions";
import getters from "@/store/modules/app/getters";

const AppModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default AppModule;
