import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/online-platforms');

const actions = {
    async fetchOnlinePlatforms({ commit }, payload) {
      const response = await http.get(uris.index(), {params: payload});

      store.commit('online_platforms/setOnlinePlatforms', response.data.data);
      store.commit('online_platforms/setOnlinePlatformsMeta', response.data.meta);
    },
    async fetchSelectOptions({ commit }, payload) {
        const response = await http.get(uris.select(), {params: payload});

        store.commit('online_platforms/setSelectOptions', response.data.data);
    },
    async storeOnlinePlatform({ commit }, payload) {
        return http.post(uris.index(), payload);
    },
    async updateOnlinePlatform({ commit }, payload) {
        return http.put(uris.show(payload.id), payload);
    },
    async deleteOnlinePlatform({ commit }, payload) {
        return http.delete(uris.show(payload.id));
    },
    async attachOnlinePlatform({ commit }, payload) {
        return http.post(uris.attach(), payload);
    },
    async detachOnlinePlatform({ commit }, payload) {
        return http.post(uris.detach(), payload);
    }
}

export default actions;
