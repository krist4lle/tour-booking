const mutations = {
    setOnlinePlatforms(state, onlinePlatforms) {
        state.onlinePlatforms = onlinePlatforms;
    },
    setOnlinePlatformsMeta(state, meta) {
        state.onlinePlatformsMeta = meta;
    },
    setSelectOptions(state, options) {
        state.selectOptions = options;
    }
}

export default mutations;
