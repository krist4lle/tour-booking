const getters = {
    onlinePlatforms(state) {
        return state.onlinePlatforms;
    },
    onlinePlatformsMeta(state) {
        return state.onlinePlatformsMeta;
    },
    selectOptions(state) {
        return state.selectOptions;
    }
}

export default getters;
