import state from "@/store/modules/online-platforms/state";
import mutations from "@/store/modules/online-platforms/mutations";
import actions from "@/store/modules/online-platforms/actions";
import getters from "@/store/modules/online-platforms/getters";

const OnlinePlatformsModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default OnlinePlatformsModule;
