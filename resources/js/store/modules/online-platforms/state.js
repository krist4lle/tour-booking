import meta from "@/store/models/meta";

const state = {
    onlinePlatforms: [],
    onlinePlatformsMeta: meta,
    selectOptions: []
}

export default state;
