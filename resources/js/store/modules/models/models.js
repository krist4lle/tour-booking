import state from "@/store/modules/models/state";
import mutations from "@/store/modules/models/mutations";
import actions from "@/store/modules/models/actions";
import getters from "@/store/modules/models/getters";

const ModelsModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default ModelsModule;
