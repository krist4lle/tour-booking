import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/app/models');

const actions = {
    async fetchSelectOptions() {
        const response = await http.get(uris.select());

        store.commit('models/setSelectOptions', response.data.data);
    },
}

export default actions;
