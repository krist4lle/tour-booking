import http from "@/Services/HttpService/http";
import getUris from "@/Services/HttpService/uris";
import {store} from "@/store/store";

const uris = getUris('/training-types');

const actions = {
    async fetchSelectOptions({ commit }, payload) {
        const response = await http.get(uris.select(), {params: payload});

        store.commit('training_types/setSelectOptions', response.data.data);
    },
}

export default actions;
