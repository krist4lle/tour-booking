import state from "@/store/modules/training-types/state";
import mutations from "@/store/modules/training-types/mutations";
import actions from "@/store/modules/training-types/actions";
import getters from "@/store/modules/training-types/getters";

const TrainingTypesModule = {
    namespaced: true,
    state: () => (state),
    mutations: mutations,
    actions: actions,
    getters: getters
}

export default TrainingTypesModule;
