const mutations = {
    setSelectOptions(state, options) {
        state.selectOptions = options;
    },
}

export default mutations;
