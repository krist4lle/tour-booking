const training = {
    id: null,
    name: null,
    description: null,
    price: null,
    online: false,
    start_datetime: null,
    duration: null,
    location: null,
    training_type_id: null,
    sport_id: null,
    trainer_id: null,
    online_platform_id: null,
    students: [],
    sport_name: null,
    first_name: null,
    training_type: null,
}

export default training;
