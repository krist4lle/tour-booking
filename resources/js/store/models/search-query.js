const searchQuery = {
    page: 1,
    per_page: 25,
    column: 'created_at',
    direction: 'desc',
    search: null
}

export default searchQuery;
