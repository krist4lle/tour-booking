const crudOptions = [
    {
        label: 'Create',
        value: 'create'
    },
    {
        label: 'Update',
        value: 'update'
    },
    {
        label: 'Delete',
        value: 'delete'
    },
    {
        label: 'View',
        value: 'view'
    },
    {
        label: 'View any',
        value: 'viewAny'
    },
    {
        label: 'Attach',
        value: 'attach'
    },
    {
        label: 'Detach',
        value: 'detach'
    },
];

export default crudOptions;
