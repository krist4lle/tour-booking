import {userProfile} from "@/store/models/user-profile";
import {role} from "@/store/models/role";

export const user = {
    id: null,
    email: null,

    profile: userProfile,
    role: role
}
