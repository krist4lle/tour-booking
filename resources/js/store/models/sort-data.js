const sortData = {
    order: 'created_at',
    sort: 'desc'
};

export default sortData;
