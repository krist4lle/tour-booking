export const userProfile = {
    avatar: null,
    bio: null,
    country: null,
    date_of_birth: null,
    first_name: null,
    full_gender: null,
    full_name: null,
    gender: null,
    last_name: null
}
