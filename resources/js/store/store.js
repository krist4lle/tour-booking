import {createStore} from "vuex";
import SportsModule from "@/store/modules/sports/sports";
import UsersModule from "@/store/modules/users/users";
import RolesModule from "@/store/modules/roles/roles";
import PermissionsModule from "@/store/modules/permissions/permissions";
import ModelsModule from "@/store/modules/models/models";
import OnlinePlatformsModule from "@/store/modules/online-platforms/online-platforms";
import TrainingsModule from "@/store/modules/trainings/trainings";
import TrainingTypesModule from "@/store/modules/training-types/training_types";
import AuthModule from "@/store/modules/auth/auth";
import AppModule from "@/store/modules/app/app";

export const store = createStore({
    modules: {
        sports: SportsModule,
        users: UsersModule,
        roles: RolesModule,
        permissions: PermissionsModule,
        models: ModelsModule,
        online_platforms: OnlinePlatformsModule,
        trainings: TrainingsModule,
        training_types: TrainingTypesModule,
        auth: AuthModule,
        app: AppModule
    }
});
