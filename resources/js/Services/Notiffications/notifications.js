import {notify} from "@kyvg/vue3-notification";

class Notifications {
    success(text, title = null) {
        notify({
            text: text,
            title: title,
            type: 'success'
        });
    }

    warn(text, title = null) {
        notify({
            text: text,
            title: title,
            type: 'warn'
        });
    }

    error(text) {
        notify({
            text: text,
            title: 'ERROR',
            type: 'error'
        });
    }
}

export default Notifications;
