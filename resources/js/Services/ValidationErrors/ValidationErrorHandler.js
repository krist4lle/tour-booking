class ValidationErrorHandler {
    errors = {};

    setErrors(errors) {
        Object.keys(errors).forEach(errorKey => {
            this.errors[errorKey] = errors[errorKey][0];
        });
    }

    dropErrors() {
        this.errors = {};
    }

    dropError(key) {
        this.errors[key] = null;
    }

    getMessage(key) {
        return this.errors[key] ? this.errors[key] : null;
    }

    hasError(key) {
        return !!this.errors[key];
    }
}

export default ValidationErrorHandler;
