class AuthService {
    admin_role = 'admin';
    student_role = 'student';
    trainer_role = 'trainer';

    isAdmin(user) {
        return user.role.name === this.admin_role;
    }

    isStudent(user) {
        return user.role.name === this.student_role;
    }

    isTrainer(user) {
        return user.role.name === this.trainer_role;
    }

    canCreate(className, permissions) {
        return this.can(className, permissions, 'create');
    }

    canUpdate(className, permissions) {
        return this.can(className, permissions, 'update');
    }

    canDelete(className, permissions) {
        return this.can(className, permissions, 'delete');
    }

    canView(className, permissions) {
        return this.can(className, permissions, 'view');
    }

    canViewAny(className, permissions) {
        return this.can(className, permissions, 'viewAny');
    }

    canAttach(className, permissions) {
        return this.can(className, permissions, 'attach');
    }

    canDetach(className, permissions) {
        return this.can(className, permissions, 'detach');
    }

    can(className, permissions, action) {
        if (Array.isArray(permissions)) {
            return !!permissions.find(permission => !!permission.includes(action + ' ' + className));
        }
        return false;
    }

    getActions(className, user, withSearch) {
        if (this.isAdmin(user)) {
            return {
                update: true,
                create: true,
                delete: true,
                view: true,
                search: withSearch
            }
        }

        const permissions = !!user.permissions ? user.permissions : [];

        return {
            update: this.canUpdate(className, permissions),
            create: this.canCreate(className, permissions),
            delete: this.canDelete(className, permissions),
            view: this.canView(className, permissions),
            search: withSearch
        }
    }
}

export const authService =  new AuthService();
