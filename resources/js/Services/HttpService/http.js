import axios from "axios";

const http = axios.create({
    baseURL: "http://localhost/api",
    headers: {
        'Accept': 'application/json'
    }
});

export default http;
