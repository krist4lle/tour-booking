function getUris(prefix) {
    return {
        index: () => prefix,
        show: (id) => prefix + '/' + id,
        select: () => prefix + '/select',
        attach: () => prefix + '/attach',
        detach: () => prefix + '/detach',
        sync: () => prefix + '/sync',
        current: () => prefix + '/current',
    };
}

export default getUris;
