class RouteHelper {
    getIdParam(path, module) {
        const index = path.indexOf(module);
        const length = module.length;
        const sum = index + length + 1;

        let result = path.slice(sum);

        if (result.includes('/')) {
            result = result.slice(result.indexOf('/'));
        }

        return result;
    }
}

export default RouteHelper
