export const headers = [
    { label: '#', width: '5%', field: 'index', isKey: true },
    { label: 'Name', width: '50%', field: 'name', sortable: true},
    { label: 'Accounts', width: '100%', field: 'users_count', sortable: true},
    { label: "Actions", width: '100%', field: "actions"},
];
