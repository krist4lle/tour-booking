export const headers = [
    { label: '#', width: '5%', field: 'index', isKey: true },
    { label: 'Name', width: '10%', field: 'name', sortable: true },
    { label: 'Guard name', width: '5%', field: 'guard_name' },
    { label: 'Model', width: '15%', field: 'class' },
    { label: 'Action type', width: '10%', field: 'action' },
    { label: 'Actions', width: '15%', field: 'actions' },
];
