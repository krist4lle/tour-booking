export const headers = [
    { label: '#', width: '3%', field: 'index' },
    { label: 'Name', width: '10%', sortable: true, field: 'name' },
    { label: 'Price', width: '5%', sortable: true, field: 'price' },
    { label: 'Sport', width: '10%', sortable: true, field: 'sport_name' },
    { label: 'Trainer', width: '10%', sortable: true, field: 'first_name' },
    { label: 'Type', width: '10%', sortable: true, field: 'training_type' },
    { label: 'Date', width: '5%', sortable: true, field: 'start_datetime', display: (value) => value.date },
    { label: 'Time', width: '5%', field: 'time', display: (value) => value.time },
    { label: 'Duration', width: '5%', sortable: true, field: 'duration' },
    { label: 'Online', width: '5%', sortable: true, field: 'online' },
    { label: 'Students', width: '3%', sortable: true, field: 'students_count' },
    { label: "Actions", width: '10%', field: 'actions' },
];
