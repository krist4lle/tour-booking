export const headers = [
    { label: '#', width: '5%', field: 'index', isKey: true },
    { label: 'Name', width: '30%', field: 'name', sortable: true},
    { label: 'Trainings', width: '30%', field: 'trainings_count', sortable: true},
    { label: 'Trainers', width: '100%', field: 'users_count', sortable: true},
    { label: "Actions", width: '100%', field: "actions"},
];
