export const headers = [
    { label: '#', width: '5%', field: 'index', isKey: true },
    { label: 'Name', width: '50%', field: 'name' },
    { label: 'Guard name', width: '50%', field: 'guard_name' },
    { label: 'Actions', width: '50%', field: 'actions' },
];
