export const headers = [
    { label: '#', width: '3%', field: 'index' },
    { label: 'First name', width: '10%', field: 'first_name' },
    { label: 'Last name', width: '10%', field: 'last_name' },
    { label: 'Email', width: '15%', field: 'email' },
    { label: 'Gender', width: '10%', field: 'gender', display: (user) => user.gender === 'M' ? 'Male' : 'Female' },
    { label: "Actions", width: '15%', field: 'actions' },
];
