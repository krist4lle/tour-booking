import {createI18n} from "vue-i18n";
import {en} from '@/lang/messages/en';
import {ua} from '@/lang/messages/ua';
import {ru} from '@/lang/messages/ru';

const locales = [
    'en',
    'ua',
    'ru'
];

export const i18n = createI18n({
    locale: getLocale(),
    availableLocales: locales,
    messages: {
        'en': en,
        'ua': ua,
        'ru': ru
    }
});

function getLocale() {
    const browserLocale = navigator.language || navigator.languages[0];

    let locale = 'en';

    if (!!browserLocale && !!locales.find(locale => locale === browserLocale)) {
        locale = locales.find(locale => locale === browserLocale);
    }

    return locale;
}
