export const ua = {
    languages: {
        en: "Англійська",
        ua: "Рідна",
        ru: "Кацапська"
    },
    layout: {
        guest: {
            login: "Увійти",
            register: "Зареєструватися",
            welcome: "Ласкаво просимо до безкоштовної програми для відстеження тренувань. Це тестова версія. Будь ласка, насолоджуйтесь використанням.",
            createAccount: "Створіть профіль!",
            getBackToTraining: "Повертайтеся до тренувань!",
            registered: "Ви успішно зареєструвалися."
        },
        auth: {
            logout: "Вийти",
            profile: "Профіль"
        }
    },
    sideBar: {
        dashboard: "Дашборд",
        trainings: "Тренування",
        sports: "Види спорту",
        settings: "Налаштування",
        users: "Користувачі"
    },
    form: {
        email: "Email",
        password: "Пароль",
        confirmPassword: "Підтвердить пароль",
        remember: "Запам'ятати мене",
        role: "Роль",
        firstName: "Ім'я",
        lastName: "Прізвище",
        gender: "Стать",
        biography: "Біографія",
        dateOfBirth: "Дата народження",
        country: "Країна",
        currentPassword: "Поточний пароль",
        newPassword: "Новий пароль",

        action: {
            save: "Зберегти",
            cancel: "Скасувати"
        },

        placeholder: {
            firstName: "Впишіть ваше ім'я",
            lastName: "Впишіть ваше прізвище",
            gender: "Оберіть стать",
            biography: "Розкажить про себе..."
        }
    },
    table: {
        navigation: {
            search: 'Пошук',
            clearFilters: "Очистити фільтри",
            createUser: "Створити користувача"
        },

        actions: "Дії",
    },
    role: {
        name: {
            admin: "Адміністратор",
            student: "Клієнт",
            trainer: "Тренер",
        }
    },
    profile: {
        information: "Ваш профіль",
        updateInformation: "Змініть інформацію вашого профілю.",
        updateEmailInfo: "Змініть вашу електронну адресу.",
        updatePassword: "Використовуйте надійний пароль.",
        delete: "Видалити акаунт",
        deleteInformation: "Після видалення вашого облікового запису всі його ресурси та дані буде остаточно видалено. Перш ніж видаляти свій обліковий запис, завантажте будь-які дані чи інформацію, які ви хочете зберегти.",
        sureToDelete: "Ви впевнені, що хочете видалити свій обліковий запис?",

        gender: {
            male: "Чоловік",
            female: "Жінка"
        }
    }
}
