export const en = {
    languages: {
        en: "English",
        ua: "Ukrainian",
        ru: "Russian"
    },
    layout: {
        guest: {
            login: "Login",
            register: "Register",
            welcome: "Welcome to free training tracker application. It is test version. Please enjoy your use.",
            createAccount: "Create an Account!",
            getBackToTraining: "Get back to training!",
            registered: "You have been successfully registered."
        },
        auth: {
            logout: "Logout",
            profile: "Profile"
        }
    },
    sideBar: {
        dashboard: "Dashboard",
        trainings: "Trainings",
        sports: "Sports",
        settings: "Settings",
        users: "Users"
    },
    form: {
        email: "Email",
        password: "Password",
        confirmPassword: "Confirm password",
        remember: "Remember me",
        role: "Role",
        firstName: "First name",
        lastName: "Last name",
        gender: "Gender",
        biography: "Biography",
        dateOfBirth: "Date of birth",
        country: "Country",
        currentPassword: "Current password",
        newPassword: "New password",

        action: {
            save: "Save",
            cancel: "Cancel"
        },

        placeholder: {
            firstName: "Enter your first name",
            lastName: "Enter your last name",
            gender: "Select gender",
            biography: "Tell us about yourself"
        }
    },
    table: {
        navigation: {
            search: 'Search',
            clearFilters: "Clear filters",
            createUser: "Create user"
        },

        actions: "Actions",
    },
    role: {
        name: {
            admin: "Admin",
            student: "Client",
            trainer: "Trainer"
        }
    },
    profile: {
        information: "Profile information",
        updateInformation: "Update your account's profile information.",
        updateEmailInfo: "Update your account's email address.",
        updatePassword: "Ensure your account is using a long, random password to stay secure.",
        delete: "Delete account",
        deleteInformation: "Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.",
        sureToDelete: "Are you sure you want to delete your account?",

        gender: {
            male: "Male",
            female: "Female"
        }
    }
}
