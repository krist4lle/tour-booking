export const ru = {
    languages: {
        en: "Английский",
        ua: "Украинский",
        ru: "Русский"
    },
    layout: {
        guest: {
            login: "Войти",
            register: "Зарегистрироваться",
            welcome: "Добро пожаловать в бесплатное приложение для отслеживания тренировок. Это тестовая версия. Пожалуйста, наслаждайтесь использованием.",
            createAccount: "Создайте профиль!",
            getBackToTraining: "Возвращайтесь к тренировкам!",
            registered: "Вы успешно зарегистрировались."
        },
        auth: {
            logout: "Выйти",
            profile: "Профиль"
        }
    },
    sideBar: {
        dashboard: "Дашборд",
        trainings: "Тренировки",
        sports: "Виды спорта",
        settings: "Натсройки",
        users: "Пользователм"
    },
    form: {
        email: "Email",
        password: "Пароль",
        confirmPassword: "Подтвердите пароль",
        remember: "Запомнить меня",
        role: "Роль",
        firstName: "Имя",
        lastName: "Фамилия",
        gender: "Пол",
        biography: "Биография",
        dateOfBirth: "Дата рождения",
        country: "Страна",
        currentPassword: "Текущий",
        newPassword: "Новый пароль",

        action: {
            save: "Сохранить",
            cancel: "Отменить"
        },

        placeholder: {
            firstName: "Введите ваше имя",
            lastName: "Введите вашу фамилию",
            gender: "Выберите пол",
            biography: "Расскажить немного о себе..."
        }
    },
    table: {
        navigation: {
            search: 'Поиск',
            clearFilters: "Очистить фильтры",
            createUser: "Создать пользователя"
        },

        actions: "Действия",
    },
    role: {
        name: {
            admin: "Администратор",
            student: "Клиент",
            trainer: "Тренер"
        }
    },
    profile: {
        information: "Ваш профиль",
        updateInformation: "Измените информацию вашего профиля.",
        updateEmailInfo: "Измените ваш электронный адрес.",
        updatePassword: "Используйте надежный пароль.",
        delete: "Удалить аккаунт",
        deleteInformation: "После удаления вашего аккаунта все его ресурсы и данные будут окончательно удалены. Прежде чем удалять свою учетную запись, загрузите какие-либо данные или информацию, которую вы хотите сохранить.",
        sureToDelete: "Вы уверены, что хотите удалить свой аккаунт?",

        gender: {
            male: "Мужчина",
            female: "Женщина"
        }
    }
}
