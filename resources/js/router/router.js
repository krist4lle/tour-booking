import { createRouter, createWebHistory } from 'vue-router'
import Login from "@/Pages/Auth/Login.vue";
import Register from "@/Pages/Auth/Register.vue";
import Welcome from "@/Pages/Auth/Welcome.vue";
import Dashboard from "@/Pages/Dashboard.vue";
import IndexUser from "@/Pages/User/Index.vue";
import IndexSport from "@/Pages/Sport/Index.vue";
import IndexMessage from "@/Pages/Message/Index.vue";
import IndexNotification from "@/Pages/Notification/Index.vue";
import ViewProfile from "@/Pages/Profile/View.vue";

const routes = [
    {
        name: 'welcome',
        path: '/',
        component: Welcome
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'profile',
        path: '/profile',
        component: ViewProfile
    },
    {
        name: 'users',
        path: '/users',
        component: IndexUser
    },
    {
        name: 'sports',
        path: '/sports',
        component: IndexSport
    },
    {
        name: 'messages',
        path: '/messages',
        component: IndexMessage
    },
    {
        name: 'notifications',
        path: '/notifications',
        component: IndexNotification
    },
];

export const router = createRouter({
    history: createWebHistory(),
    routes: routes
});
