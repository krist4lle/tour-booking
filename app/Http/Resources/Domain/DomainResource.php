<?php

namespace App\Http\Resources\Domain;

use App\DTO\Domain\DomainDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DomainResource extends JsonResource
{
    /** @var DomainDTO */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'class' => $this->resource->getClass(),
            'name' => $this->resource->getName(),
        ];
    }
}
