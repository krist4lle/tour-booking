<?php

namespace App\Http\Resources\Domain;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DomainCollection extends ResourceCollection
{
    public $collects = DomainResource::class;
}
