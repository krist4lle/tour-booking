<?php

namespace App\Http\Resources\Permission;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\Permission\Models\Permission;

class PermissionResource extends JsonResource
{
    /** @var Permission */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $permission = explode(' ', $this->resource->name);
        $domain = $permission[1];
        $action = $permission[0];

        return [
            'id' => $this->resource->id,
            'name' => ucfirst($action).' '.class_basename($domain),
            'guard_name' => $this->resource->guard_name,

            'action' => $action,
            'domain' => $domain,
        ];
    }
}
