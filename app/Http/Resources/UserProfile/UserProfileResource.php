<?php

namespace App\Http\Resources\UserProfile;

use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /** @var UserProfile */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'first_name' => $this->resource->first_name,
            'last_name' => $this->resource->last_name,
            'bio' => $this->resource->bio,
            'date_of_birth' => $this->resource->date_of_birth,
            'country' => $this->resource->country,
            'avatar' => $this->resource->avatar,
            'gender' => $this->resource->gender,
            'full_gender' => $this->resource->fullGender,
            'full_name' => $this->resource->fullName
        ];
    }
}
