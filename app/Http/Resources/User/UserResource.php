<?php

namespace App\Http\Resources\User;

use App\Exceptions\UserException;
use App\Http\Resources\OnlinePlatform\OnlinePlatformResource;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Sport\SportCollection;
use App\Http\Resources\Training\TrainingCollection;
use App\Http\Resources\UserProfile\UserProfileResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class UserResource extends JsonResource
{
    /** @var User */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     *
     * @throws UserException
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'email' => $this->resource->email,

            'role' => new RoleResource($this->resource->getRole()),
            'profile' => new UserProfileResource($this->resource->profile),
            'online_platforms' => OnlinePlatformResource::collection($this->resource->onlinePlatforms),
            'sports' => new SportCollection($this->resource->sports),
            'trainings' => new TrainingCollection($this->getTrainings()),
        ];
    }

    private function getTrainings(): Collection
    {
        if ($this->resource->isTrainer()) {
            $trainings = $this->resource->trainings;
        } else {
            $trainings = $this->resource->studentsTrainings;
        }

        return $trainings;
    }
}
