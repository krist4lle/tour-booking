<?php

namespace App\Http\Resources\Sport;

use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SportResource extends JsonResource
{
    /** @var Sport */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'users_count' => $this->resource->users_count ?? null,
            'trainings_count' => $this->resource->trainings_count ?? null,
        ];
    }
}
