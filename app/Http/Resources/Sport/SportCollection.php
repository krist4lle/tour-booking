<?php

namespace App\Http\Resources\Sport;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SportCollection extends ResourceCollection
{
    public $collects = SportResource::class;
}
