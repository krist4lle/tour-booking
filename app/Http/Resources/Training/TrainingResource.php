<?php

namespace App\Http\Resources\Training;

use App\Http\Resources\OnlinePlatform\OnlinePlatformResource;
use App\Http\Resources\Sport\SportResource;
use App\Http\Resources\TrainingType\TrainingTypeResource;
use App\Http\Resources\User\UserResource;
use App\Models\Training;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingResource extends JsonResource
{
    /** @var Training */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'date' => $this->resource->start_datetime->format('d.m.Y'),
            'time' => $this->resource->start_datetime->format('H:i'),
            'start_datetime' => $this->resource->start_datetime,
            'duration' => $this->resource->duration,
            'price' => $this->resource->price,
            'online' => $this->resource->online,
            'description' => $this->resource->description,
            'location' => $this->resource->location,
            'students_count' => $this->resource->students_count ?? null,

            //            'sport' => new SportResource($this->resource->sport),
            //            'trainer' => new UserResource($this->resource->trainer),
            //            'training_type' => new TrainingTypeResource($this->resource->trainingType),
            //            'online_platform' => new OnlinePlatformResource($this->resource->onlinePlatform)
        ];
    }
}
