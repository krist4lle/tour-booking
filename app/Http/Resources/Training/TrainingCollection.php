<?php

namespace App\Http\Resources\Training;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TrainingCollection extends ResourceCollection
{
    public $collects = TrainingResource::class;
}
