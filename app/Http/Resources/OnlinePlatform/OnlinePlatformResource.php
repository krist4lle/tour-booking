<?php

namespace App\Http\Resources\OnlinePlatform;

use App\Models\OnlinePlatform;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OnlinePlatformResource extends JsonResource
{
    /** @var OnlinePlatform */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'slug' => $this->resource->slug,
            'users_count' => $this->resource->users_count ?? null,
            'account' => $this->resource->pivot->account ?? null,
        ];
    }
}
