<?php

namespace App\Http\Resources\OnlinePlatform;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OnlinePlatformCollection extends ResourceCollection
{
    public $collects = OnlinePlatformResource::class;
}
