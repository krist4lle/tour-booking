<?php

namespace App\Http\Resources\TrainingType;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TrainingTypeCollection extends ResourceCollection
{
    public $collects = TrainingResource::class;
}
