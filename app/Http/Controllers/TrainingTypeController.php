<?php

namespace App\Http\Controllers;

use App\Http\Resources\TrainingType\TrainingTypeCollection;
use App\Services\Training\TrainingServiceContract;
use App\Traits\HasJsonResponse;

class TrainingTypeController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly TrainingServiceContract $trainingService
    ) {
    }

    public function index(): TrainingTypeCollection
    {
        $trainingTypes = $this->trainingService->getTrainingTypes();

        return new TrainingTypeCollection($trainingTypes);
    }
}
