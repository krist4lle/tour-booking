<?php

namespace App\Http\Controllers;

use App\Http\Requests\Telegram\TelegramWebhookRequest;
use App\Services\Telegram\TelegramServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class TelegramController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly TelegramServiceContract $telegramService
    ) {
    }

    public function handleWebhook(TelegramWebhookRequest $request): JsonResponse
    {
        //Log::info(print_r($request->all(), true));

        $this->telegramService->handleWebhook($request->createTelegramRequestDTO());

        return $this->response(code: 201);
    }
}
