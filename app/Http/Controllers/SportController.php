<?php

namespace App\Http\Controllers;

use App\Http\Requests\Sport\AttachDetachSportRequest;
use App\Http\Requests\Sport\SportCreateRequest;
use App\Http\Requests\Sport\SportIndexRequest;
use App\Http\Resources\Sport\SportCollection;
use App\Http\Resources\Sport\SportResource;
use App\Models\Sport;
use App\Services\Training\TrainingServiceContract;
use App\Services\User\UserServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SportController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly TrainingServiceContract $trainingService,
        private readonly UserServiceContract $userService
    ) {
    }

    public function index(SportIndexRequest $request): SportCollection
    {
        $this->authorize('viewAny', Sport::class);

        $sports = $this->trainingService->getSports($request->createSortingDTO(), $request->createPaginationDTO());

        return new SportCollection($sports);
    }

    public function store(SportCreateRequest $request): SportResource
    {
        $this->authorize('create', Sport::class);

        $sport = $this->trainingService->storeSport($request->createSportDTO());

        return new SportResource($sport);
    }

    public function update(SportCreateRequest $request, Sport $sport): SportResource
    {
        $this->authorize('update', $sport);

        $sport = $this->trainingService->updateSport($request->createSportDTO(), $sport);

        return new SportResource($sport);
    }

    public function delete(Request $request, Sport $sport): JsonResponse
    {
        $this->authorize('delete', $sport);

        $this->trainingService->deleteSport($sport);

        return $this->response(code: 204);
    }

    public function attach(AttachDetachSportRequest $request): JsonResponse
    {
        $sport = $this->trainingService->getSportById($request->get('sport_id'));
        $user = $this->userService->getUserById($request->get('user_id'));

        $this->authorize('attach', [$sport, $user]);

        $this->userService->attachSportToUser($user, $sport);

        return $this->response();
    }

    public function detach(AttachDetachSportRequest $request): JsonResponse
    {
        $sport = $this->trainingService->getSportById($request->get('sport_id'));
        $user = $this->userService->getUserById($request->get('user_id'));

        $this->authorize('detach', [$sport, $user]);

        $this->userService->detachSportFromUser($user, $sport);

        return $this->response();
    }
}
