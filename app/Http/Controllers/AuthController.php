<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Services\Auth\AuthServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly AuthServiceContract $authService
    ) {
    }

    public function register(RegisterRequest $request): UserResource
    {
        $user = $this->authService->registerUser($request->createUserDTO());

        return new UserResource($user);
    }

    public function login(LoginRequest $request): JsonResponse|RedirectResponse
    {
        $this->authService->loginUser($request->createLoginDTO(), 'api');

        return $this->response(
            ['token' => Auth::user()->createToken('api')->plainTextToken],
            'You have been logged in.'
        );
    }

    public function logout(): JsonResponse
    {
        $this->authService->logout();

        return $this->response();
    }
}
