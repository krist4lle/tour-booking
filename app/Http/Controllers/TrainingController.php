<?php

namespace App\Http\Controllers;

use App\Http\Requests\Training\AttachDetachTrainingRequest;
use App\Http\Requests\Training\TrainingIndexRequest;
use App\Http\Requests\Training\TrainingStoreRequest;
use App\Http\Resources\Training\TrainingCollection;
use App\Http\Resources\Training\TrainingResource;
use App\Models\Training;
use App\Services\Training\TrainingServiceContract;
use App\Services\User\UserServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TrainingController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly TrainingServiceContract $trainingService,
        private readonly UserServiceContract $userService
    ) {
    }

    public function index(TrainingIndexRequest $request): TrainingCollection
    {
        $this->authorize('viewAny', Training::class);

        $trainings = $this->trainingService->getTrainings(
            $request->createTrainingFiltersDTO(),
            $request->createSortingDTO(),
            $request->createPaginationDTO()
        );

        return new TrainingCollection($trainings);
    }

    public function show(Request $request, Training $training): TrainingResource
    {
        $this->authorize('view', $training);

        return new TrainingResource($training);
    }

    public function store(TrainingStoreRequest $request): TrainingResource
    {
        $this->authorize('create', Training::class);

        $training = $this->trainingService->storeTraining($request->createTrainingDTO());

        return new TrainingResource($training);
    }

    public function update(TrainingStoreRequest $request, training $training): TrainingResource
    {
        $this->authorize('update', $training);

        $this->trainingService->updateTraining($request->createTrainingDTO(), $training);

        return new TrainingResource($training);
    }

    public function destroy(Request $request, Training $training): JsonResponse
    {
        $this->authorize('delete', $training);

        $this->trainingService->deleteTraining($training);

        return $this->response(code: 204);
    }

    public function attach(AttachDetachTrainingRequest $request): JsonResponse
    {
        $this->authorize('attach', [
            $this->trainingService->getTraining($request->get('training_id')),
            $this->userService->getUser($request->get('user_id')),
        ]);

        $this->userService->attachUserToTraining($request->validated());

        return $this->response(message: 'User has been attached to Training.');
    }

    public function detach(AttachDetachTrainingRequest $request): JsonResponse
    {
        $this->authorize('detach', [
            $this->trainingService->getTraining($request->get('training_id')),
            $this->userService->getUser($request->get('user_id')),
        ]);

        $this->userService->detachUserFromTraining($request->validated());

        return $this->response(message: 'User has been detached from Training.');
    }
}
