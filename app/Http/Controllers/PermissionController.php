<?php

namespace App\Http\Controllers;

use App\Http\Requests\Permission\PermissionIndexRequest;
use App\Http\Requests\Permission\PermissionStoreRequest;
use App\Http\Resources\Permission\PermissionCollection;
use App\Http\Resources\Permission\PermissionResource;
use App\Services\RolePermission\RolePermissionServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly RolePermissionServiceContract $rolePermissionService
    ) {
    }

    public function index(PermissionIndexRequest $request): PermissionCollection
    {
        $this->authorize('viewAny', Permission::class);

        $permissions = $this->rolePermissionService->getPermissions(
            $request->createPermissionFiltersDTO(),
            $request->createSortingDTO(),
            $request->createPaginationDTO()
        );

        return new PermissionCollection($permissions);
    }

    public function store(PermissionStoreRequest $request): PermissionResource
    {
        $this->authorize('create', Permission::class);

        $permission = $this->rolePermissionService->storePermission($request->createPermissionDTO());

        return new PermissionResource($permission);
    }

    public function update(PermissionStoreRequest $request, Permission $permission): PermissionResource
    {
        $this->authorize('update', $permission);

        $permission = $this->rolePermissionService->updatePermission($request->createPermissionDTO(), $permission);

        return new PermissionResource($permission);
    }

    public function delete(Request $request, Permission $permission): JsonResponse
    {
        $this->authorize('delete', $permission);

        $this->rolePermissionService->deletePermission($permission);

        return $this->response(code: 204);
    }
}
