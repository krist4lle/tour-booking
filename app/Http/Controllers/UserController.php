<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserIndexRequest;
use App\Http\Requests\User\UserLocaleRequest;
use App\Http\Requests\User\UserPasswordUpdateRequest;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\User\UserServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class UserController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly UserServiceContract $userService
    ) {
    }

    public function current(): UserResource|JsonResponse
    {
        return \Auth::user() ? new UserResource(\Auth::user()) : $this->response();
    }

    public function index(UserIndexRequest $request): UserCollection
    {
        $this->authorize('viewAny', User::class);

        $users = $this->userService->getUsers(
            $request->createUserFiltersDTO(),
            $request->createSortingDTO(),
            $request->createPaginationDTO()
        );

        return new UserCollection($users);
    }

    public function show(Request $request, User $user): UserResource
    {
        $this->authorize('view', $user);

        return new UserResource($user);
    }

    public function store(UserStoreRequest $request): UserResource
    {
        $this->authorize('create', User::class);

        $user = $this->userService->createUser($request->createUserDTO(), $request->createUserProfileDTO());

        return new UserResource($user);
    }

    public function update(UserUpdateRequest $request, User $user): UserResource
    {
        $this->authorize('update', $user);

        $user = $this->userService->updateUser($request->createUserDTO(), $request->createUserProfileDTO(), $user);

        return new UserResource($user);
    }

    public function updatePassword(UserPasswordUpdateRequest $request): UserResource
    {
        $this->authorize('update', User::class);

        $user = $this->userService->updateUserPassword($request->password, \Auth::user());

        return new UserResource($user);
    }

    public function delete(Request $request, User $user): JsonResponse
    {
        $this->authorize('delete', User::class);

        $this->userService->deleteUser($user);

        return $this->response(code: 204);
    }

    public function setLocale(UserLocaleRequest $request): void
    {
        App::setLocale($request->get('locale'));
    }
}
