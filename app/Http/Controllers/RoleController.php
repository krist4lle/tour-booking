<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\RoleSyncRequest;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Role\RoleResource;
use App\Services\RolePermission\RolePermissionServiceContract;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct(
        private readonly RolePermissionServiceContract $rolePermissionService
    ) {
    }

    public function index(): RoleCollection
    {
        return new RoleCollection($this->rolePermissionService->getRoles());
    }

    public function show(Request $request, Role $role): RoleResource
    {
        $this->authorize('view', $role);

        return new RoleResource($role);
    }

    public function sync(RoleSyncRequest $request): RoleResource
    {
        $role = $this->rolePermissionService->getRoleById($request->get('role_id'));

        $this->authorize('attach', [$role, $request->get('permissions')]);

        $this->rolePermissionService->syncPermissionsWithRole($role, $request->get('permissions'));

        return new RoleResource($role);
    }
}
