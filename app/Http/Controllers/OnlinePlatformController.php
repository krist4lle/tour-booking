<?php

namespace App\Http\Controllers;

use App\Http\Requests\OnlinePlatform\AttachOnlinePlatformRequest;
use App\Http\Requests\OnlinePlatform\DetachOnlinePlatformRequest;
use App\Http\Requests\OnlinePlatform\OnlinePlatformIndexRequest;
use App\Http\Requests\OnlinePlatform\OnlinePlatformStoreRequest;
use App\Http\Resources\OnlinePlatform\OnlinePlatformCollection;
use App\Http\Resources\OnlinePlatform\OnlinePlatformResource;
use App\Models\OnlinePlatform;
use App\Services\User\UserServiceContract;
use App\Traits\HasJsonResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OnlinePlatformController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly UserServiceContract $userService
    ) {
    }

    public function index(OnlinePlatformIndexRequest $request): OnlinePlatformCollection
    {
        $this->authorize('viewAny', OnlinePlatform::class);

        $onlinePlatforms = $this->userService->getOnlinePlatforms(
            $request->createSortingDTO(),
            $request->createPaginationDTO()
        );

        return new OnlinePlatformCollection($onlinePlatforms);
    }

    public function store(OnlinePlatformStoreRequest $request): OnlinePlatformResource
    {
        $this->authorize('create', OnlinePlatform::class);

        $platform = $this->userService->storeOnlinePlatform($request->createOnlinePlatformDTO());

        return new OnlinePlatformResource($platform);
    }

    public function update(OnlinePlatformStoreRequest $request, OnlinePlatform $onlinePlatform): OnlinePlatformResource
    {
        $this->authorize('update', $onlinePlatform);

        $platform = $this->userService->updateOnlinePlatform($request->createOnlinePlatformDTO(), $onlinePlatform);

        return new OnlinePlatformResource($platform);
    }

    public function delete(Request $request, OnlinePlatform $onlinePlatform): JsonResponse
    {
        $this->authorize('delete', $onlinePlatform);

        $this->userService->deleteOnlinePlatform($onlinePlatform);

        return $this->response(code: 204);
    }

    public function attach(AttachOnlinePlatformRequest $request): JsonResponse
    {
        $user = $this->userService->getUserById($request->get('user_id'));
        $onlinePlatform = $this->userService->getOnlinePlatformById($request->get('online_platform_id'));

        $this->authorize('attach', [$onlinePlatform, $user]);

        $this->userService->attachOnlinePlatformToUser($user, $onlinePlatform, $request->get('account'));

        return $this->response();
    }

    public function detach(DetachOnlinePlatformRequest $request): JsonResponse
    {
        $user = $this->userService->getUserById($request->get('user_id'));
        $onlinePlatform = $this->userService->getOnlinePlatformById($request->get('online_platform_id'));

        $this->authorize('detach', [$onlinePlatform, $user]);

        $this->userService->detachOnlinePlatformFromUser($user, $onlinePlatform);

        return $this->response();
    }
}
