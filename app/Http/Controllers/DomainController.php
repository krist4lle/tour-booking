<?php

namespace App\Http\Controllers;

use App\Http\Resources\Domain\DomainCollection;
use App\Services\RolePermission\RolePermissionServiceContract;
use App\Traits\HasJsonResponse;
use Spatie\Permission\Models\Permission;

class DomainController extends Controller
{
    use HasJsonResponse;

    public function __construct(
        private readonly RolePermissionServiceContract $rolePermissionService
    ) {
    }

    public function index(): DomainCollection
    {
        $this->authorize('viewAny', Permission::class);

        $domains = $this->rolePermissionService->getDomains();

        return new DomainCollection($domains);
    }
}
