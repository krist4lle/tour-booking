<?php

namespace App\Http\Requests\Permission;

use App\DTO\Permission\PermissionDTO;
use App\Rules\InActionsList;
use App\Rules\InDomainsList;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $domain
 * @property string $action
 */
class PermissionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'domain' => [
                'required',
                'string',
                new InDomainsList,
            ],
            'action' => [
                'required',
                'string',
                new InActionsList,
            ],
        ];
    }

    public function createPermissionDTO(): PermissionDTO
    {
        return new PermissionDTO([
            'domain' => $this->domain,
            'action' => $this->action,
        ]);
    }
}
