<?php

namespace App\Http\Requests\Permission;

use App\DTO\Permission\PermissionFiltersDTO;
use App\Traits\RequestHasPagination;
use App\Traits\RequestHasSorting;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string|null $search
 * @property int|null $role_id
 */
class PermissionIndexRequest extends FormRequest
{
    use RequestHasSorting, RequestHasPagination;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'search' => 'nullable|string',
            'role_id' => 'nullable|integer|exists:roles,id',

            'column' => 'nullable|string|in:created_at,name',
            'direction' => 'nullable|string|in:asc,desc',

            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer',
        ];
    }

    public function createPermissionFiltersDTO(): PermissionFiltersDTO
    {
        return new PermissionFiltersDTO([
            'search' => $this->search ?? null,
            'roleId' => $this->role_id ?? null,
        ]);
    }
}
