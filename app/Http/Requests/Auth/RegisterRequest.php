<?php

namespace App\Http\Requests\Auth;

use App\DTO\User\UserDTO;
use App\Rules\RolesWithoutAdmin;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $email
 * @property string $password
 * @property int $role_id
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|confirmed|min:6',
            'password_confirmation' => 'required|string',
            'role_id' => [
                'required',
                'integer',
                'exists:roles,id',
                new RolesWithoutAdmin,
            ],
        ];
    }

    public function createUserDTO(): UserDTO
    {
        return new UserDTO([
            'email' => $this->email,
            'password' => $this->password,
            'roleId' => $this->role_id,
        ]);
    }
}
