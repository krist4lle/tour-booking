<?php

namespace App\Http\Requests\Telegram;

use App\DTO\Telegram\TelegramRequestDTO;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class TelegramWebhookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'message' => 'nullable|array',

            'message.text' => 'required_with:message|string',

            'message.chat' => 'required_with:message|array',
            'message.chat.id' => 'required_with:message|integer',
            'message.chat.first_name' => 'nullable|string',
            'message.chat.last_name' => 'nullable|string',

            'message.from' => 'required_with:message|array',
            'message.from.id' => 'required_with:message|integer',
            'message.from.first_name' => 'nullable|string',
            'message.from.last_name' => 'nullable|string',
            'message.from.username' => 'nullable|string',
            'message.from.language_code' => 'nullable|string',

            'callback_query' => 'nullable|array',

            'callback_query.data' => 'required_with:callback_query|string',

            'callback_query.from' => 'required_with:callback_query|array',
            'callback_query.from.id' => 'required_with:callback_query|integer',
            'callback_query.from.first_name' => 'nullable|string',
            'callback_query.from.last_name' => 'nullable|string',
            'callback_query.from.username' => 'nullable|string',
            'callback_query.from.language_code' => 'nullable|string',

            'callback_query.message' => 'required_with:callback_query|array',

            'callback_query.message.text' => 'required_with:callback_query|string',

            'callback_query.message.chat' => 'required_with:callback_query|array',
            'callback_query.message.chat.id' => 'required_with:callback_query|integer',
            'callback_query.message.chat.first_name' => 'nullable|string',
            'callback_query.message.chat.last_name' => 'nullable|string',

            'callback_query.message.from' => 'required_with:callback_query|array',
            'callback_query.message.from.id' => 'required_with:callback_query|integer',
            'callback_query.message.from.first_name' => 'nullable|string',
            'callback_query.message.from.last_name' => 'nullable|string',
            'callback_query.message.from.username' => 'nullable|string',
            'callback_query.message.from.language_code' => 'nullable|string',
        ];
    }

    public function createTelegramRequestDTO(): ?TelegramRequestDTO
    {
        return new TelegramRequestDTO($this->all());
    }
}
