<?php

namespace App\Http\Requests\Training;

use App\DTO\Training\TrainingDTO;
use App\Models\User;
use App\Rules\IsSameTrainer;
use App\Rules\TrainerHasSport;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property int $sport_id
 * @property int $trainer_id
 * @property int $training_type_id
 * @property string $name
 * @property string|null $description
 * @property float|null $price
 * @property bool $online
 * @property string|null $location
 * @property int|null $online_platform_id
 * @property string $start_datetime
 * @property int $duration
 */
class TrainingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        /** @var User $user */
        $user = \Auth::user()->loadMissing(['profile', 'sports']);

        return [
            'sport_id' => [
                'required',
                'integer',
                'exists:sports,id',
                new TrainerHasSport($user),
            ],
            'trainer_id' => [
                'required',
                'integer',
                'exists:users,id',
                new IsSameTrainer($user),
            ],
            'training_type_id' => 'required|integer|exists:training_types,id',

            'name' => 'required|string',
            'description' => 'nullable|string',
            'price' => 'nullable|decimal:0,2',
            'online' => 'required|boolean',
            // Location depends on online status
            'location' => 'nullable|required_if:online,false|string',
            'online_platform_id' => 'nullable|required_if:online,true|integer|exists:online_platforms,id',

            'start_datetime' => 'required|date',
            'duration' => 'required|integer|min:0',
        ];
    }

    public function createTrainingDTO(): TrainingDTO
    {
        return new TrainingDTO([
            'sportId' => $this->sport_id,
            'trainerId' => $this->trainer_id,
            'trainingTypeId' => $this->training_type_id,
            'name' => $this->name,
            'description' => $this->description ?? null,
            'price' => $this->price ?? 0,
            'online' => $this->online,
            'location' => $this->location ?? null,
            'onlinePlatformId' => $this->online_platform_id ?? null,
            'startDatetime' => $this->start_datetime,
            'duration' => $this->duration,
        ]);
    }
}
