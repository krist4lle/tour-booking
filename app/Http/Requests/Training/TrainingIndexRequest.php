<?php

namespace App\Http\Requests\Training;

use App\DTO\Training\TrainingFiltersDTO;
use App\Models\Training;
use App\Traits\RequestHasPagination;
use App\Traits\RequestHasSorting;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string|null $search
 * @property int|null $sport_id
 * @property int|null $trainer_id
 * @property int|null $training_type_id
 * @property string|null $from
 * @property string|null $to
 */
class TrainingIndexRequest extends FormRequest
{
    use RequestHasSorting, RequestHasPagination;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'search' => 'nullable|string',
            'sport_id' => 'nullable|integer|exists:sports,id',
            'trainer_id' => 'nullable|integer|exists:users,id',
            'training_type_id' => 'nullable|integer|exists:training_types,id',
            'from' => 'nullable|date',
            'to' => 'nullable|date',

            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer',

            'column' => 'nullable|string|in:'.implode(',', Training::SORTABLE_COLUMNS),
            'direction' => 'nullable|string|in:asc,desc',
        ];
    }

    public function createTrainingFiltersDTO(): TrainingFiltersDTO
    {
        return new TrainingFiltersDTO([
            'search' => $this->search ?? null,
            'sportId' => $this->sport_id ?? null,
            'trainerId' => $this->trainer_id ?? null,
            'trainingTypeId' => $this->training_type_id ?? null,
            'from' => $this->from ?? null,
            'to' => $this->to ?? null,
        ]);
    }
}
