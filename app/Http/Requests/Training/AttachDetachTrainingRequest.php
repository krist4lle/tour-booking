<?php

namespace App\Http\Requests\Training;

use Illuminate\Foundation\Http\FormRequest;

class AttachDetachTrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'training_id' => 'required|integer|exists:trainings,id',
            'user_id' => 'required|integer|exists:users,id',
        ];
    }
}
