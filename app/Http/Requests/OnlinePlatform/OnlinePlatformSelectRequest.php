<?php

namespace App\Http\Requests\OnlinePlatform;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $sport_id
 */
class OnlinePlatformSelectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'online_platforms_ids' => 'nullable|array',
            'online_platforms_ids.*' => 'required_with:sports_ids|integer|exists:online_platforms,id',
        ];
    }
}
