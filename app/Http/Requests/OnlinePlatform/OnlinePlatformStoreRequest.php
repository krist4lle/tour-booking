<?php

namespace App\Http\Requests\OnlinePlatform;

use App\DTO\OnlinePlatform\OnlinePlatformDTO;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $name
 */
class OnlinePlatformStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|unique:online_platforms,name',
        ];
    }

    public function createOnlinePlatformDTO(): OnlinePlatformDTO
    {
        return new OnlinePlatformDTO($this->name);
    }
}
