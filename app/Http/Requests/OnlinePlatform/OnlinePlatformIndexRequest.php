<?php

namespace App\Http\Requests\OnlinePlatform;

use App\Models\OnlinePlatform;
use App\Traits\RequestHasPagination;
use App\Traits\RequestHasSorting;
use Illuminate\Foundation\Http\FormRequest;

class OnlinePlatformIndexRequest extends FormRequest
{
    use RequestHasSorting, RequestHasPagination;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // Sorting
            'column' => 'nullable|string|in:'.implode(',', OnlinePlatform::SORTABLE_COLUMNS),
            'direction' => 'nullable|string|in:asc,desc',

            // Meta
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer',
        ];
    }
}
