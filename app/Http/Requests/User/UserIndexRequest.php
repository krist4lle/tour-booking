<?php

namespace App\Http\Requests\User;

use App\DTO\User\UserFiltersDTO;
use App\Models\User;
use App\Traits\RequestHasPagination;
use App\Traits\RequestHasSorting;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string|null $search
 * @property int|null $role_id
 * @property string|null $gender
 */
class UserIndexRequest extends FormRequest
{
    use RequestHasPagination, RequestHasSorting;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            // Filters
            'search' => 'nullable|string',
            'role_id' => 'nullable|integer|exists:roles,id',
            'gender' => 'nullable|string|in:M,F',

            // Sorting
            'column' => 'nullable|string|in:'.implode(',', User::SORTABLE_COLUMNS),
            'direction' => 'nullable|string|in:asc,desc',

            // Meta
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer',
        ];
    }

    public function createUserFiltersDTO(): UserFiltersDTO
    {
        return new UserFiltersDTO([
            'search' => $this->search ?? null,
            'roleId' => $this->role_id ?? null,
            'gender' => $this->gender ?? null,
        ]);
    }
}
