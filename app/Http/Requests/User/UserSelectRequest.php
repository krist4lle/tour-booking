<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $sport_id
 */
class UserSelectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'role' => 'nullable|string|exists:roles,name',
            'users_ids' => 'nullable|array',
            'users_ids.*' => 'required_with:users_ids|integer|exists:users,id',
        ];
    }
}
