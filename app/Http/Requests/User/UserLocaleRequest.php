<?php

namespace App\Http\Requests\User;

use App\DTO\User\UserFiltersDTO;
use App\Models\User;
use App\Traits\RequestHasPagination;
use App\Traits\RequestHasSorting;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string|null $search
 * @property int|null $role_id
 * @property string|null $gender
 */
class UserLocaleRequest extends FormRequest
{
    use RequestHasPagination, RequestHasSorting;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'locale' => 'required|string|in:en,ru,ua'
        ];
    }
}
