<?php

namespace App\Http\Requests\User;

use App\DTO\User\UserDTO;
use App\DTO\User\UserProfileDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $email
 * @property int $role_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $gender
 * @property string|null $bio
 * @property string|null $country
 * @property string|null $date_of_birth
 */
class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|unique:users,email',
            'role_id' => 'required|integer|exists:roles,id',

            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'nullable|string|in:F,M',
            'bio' => 'nullable|string',
            'country' => 'nullable|string',
            'date_of_birth' => 'nullable|date|before:today',
        ];
    }

    public function createUserDTO(): UserDTO
    {
        return new UserDTO([
            'email' => $this->email,
            'password' => \Str::random(12),
            'roleId' => $this->role_id,
        ]);
    }

    public function createUserProfileDTO(): UserProfileDTO
    {
        return new UserProfileDTO([
            'firstName' => $this->first_name ?? null,
            'lastName' => $this->last_name ?? null,
            'gender' => $this->gender ?? null,
            'bio' => $this->bio ?? null,
            'country' => $this->country ?? null,
            'dateOfBirth' => $this->date_of_birth ?? null,
        ]);
    }
}
