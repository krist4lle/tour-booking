<?php

namespace App\Http\Requests\Sport;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $sport_id
 */
class SportSelectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'sports_ids' => 'nullable|array',
            'sports_ids.*' => 'required_with:sports_ids|integer|exists:sports,id',
        ];
    }
}
