<?php

namespace App\Http\Requests\Sport;

use App\DTO\Sport\SportDTO;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $name
 */
class SportCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|unique:sports,name',
        ];
    }

    public function createSportDTO(): SportDTO
    {
        return new SportDTO($this->name);
    }
}
