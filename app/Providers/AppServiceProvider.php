<?php

namespace App\Providers;

use App\Services\Auth\AuthService;
use App\Services\Auth\AuthServiceContract;
use App\Services\RolePermission\RolePermissionService;
use App\Services\RolePermission\RolePermissionServiceContract;
use App\Services\Telegram\TelegramService;
use App\Services\Telegram\TelegramServiceContract;
use App\Services\Training\TrainingService;
use App\Services\Training\TrainingServiceContract;
use App\Services\User\UserService;
use App\Services\User\UserServiceContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(RolePermissionServiceContract::class, RolePermissionService::class);
        $this->app->bind(UserServiceContract::class, UserService::class);
        $this->app->bind(TrainingServiceContract::class, TrainingService::class);
        $this->app->bind(AuthServiceContract::class, AuthService::class);
        $this->app->bind(TelegramServiceContract::class, TelegramService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
