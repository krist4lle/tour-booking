<?php

namespace App\Providers;

use App\Repositories\OnlinePlatform\OnlinePlatformRepository;
use App\Repositories\OnlinePlatform\OnlinePlatformRepositoryContract;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryContract;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Sport\SportRepository;
use App\Repositories\Sport\SportRepositoryContract;
use App\Repositories\Training\TrainingRepository;
use App\Repositories\Training\TrainingRepositoryContract;
use App\Repositories\TrainingType\TrainingTypeRepository;
use App\Repositories\TrainingType\TrainingTypeRepositoryContract;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(RoleRepositoryContract::class, RoleRepository::class);
        $this->app->bind(PermissionRepositoryContract::class, PermissionRepository::class);
        $this->app->bind(UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(OnlinePlatformRepositoryContract::class, OnlinePlatformRepository::class);
        $this->app->bind(SportRepositoryContract::class, SportRepository::class);
        $this->app->bind(TrainingTypeRepositoryContract::class, TrainingTypeRepository::class);
        $this->app->bind(TrainingRepositoryContract::class, TrainingRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
