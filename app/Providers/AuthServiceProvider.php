<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\Training;
use App\Models\User;
use App\Policies\OnlinePlatformPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;
use App\Policies\SportPolicy;
use App\Policies\TrainingPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Sport::class => SportPolicy::class,
        OnlinePlatform::class => OnlinePlatformPolicy::class,
        Training::class => TrainingPolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Gate::before(function (User $user, string $ability) {
            return $user->hasRole(User::ROLE_ADMIN) ? true : null;
        });
    }
}
