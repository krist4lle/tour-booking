<?php

namespace App\Observers;

use App\Models\User;
use App\Services\User\UserServiceContract;

class UserObserver
{
    public function __construct(
        private readonly UserServiceContract $userService
    ) {
    }

    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        if (isset($user->telegram_username)) {
            $onlinePlatform = $this->userService->getOnlinePlatformBySlug('telegram');

            $this->userService->attachOnlinePlatformToUser($user, $onlinePlatform, "@{$user->telegram_username}");
        }
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
