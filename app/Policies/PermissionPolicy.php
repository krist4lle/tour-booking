<?php

namespace App\Policies;

use App\Models\User;
use App\Traits\GetPermissionName;
use Spatie\Permission\Models\Permission;

class PermissionPolicy
{
    use GetPermissionName;

    private string $className;

    public function __construct()
    {
        $this->className = Permission::class;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Permission $permissionObject): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Permission $permissionObject): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Permission $permissionObject): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }
}
