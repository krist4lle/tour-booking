<?php

namespace App\Policies;

use App\Models\Training;
use App\Models\User;
use App\Traits\GetPermissionName;

class TrainingPolicy
{
    use GetPermissionName;

    private string $className;

    public function __construct()
    {
        $this->className = Training::class;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Training $training): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {
            if ($user->isTrainer() && $training->trainer_id !== $user->id) {

                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Training $training): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission) && $training->trainer_id === $user->id) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Training $training): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission) && $training->trainer_id === $user->id) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can attach Training to User.
     */
    public function attach(User $user, Training $training, User $model): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission) && $model->isStudent()) {
            if ($user->isTrainer() && $training->trainer_id !== $user->id) {

                return false;
            }

            if ($user->isStudent() && $user->id !== $model->id) {

                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can detach Training from User.
     */
    public function detach(User $user, Training $training, User $model): bool
    {
        return $this->attach($user, $training, $model);
    }
}
