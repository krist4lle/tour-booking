<?php

namespace App\Policies;

use App\Models\User;
use App\Traits\GetPermissionName;
use Spatie\Permission\Models\Role;

class RolePolicy
{
    use GetPermissionName;

    private string $className;

    public function __construct()
    {
        $this->className = Role::class;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Role $role): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Role $role): bool
    {
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Role $role): bool
    {
        return false;
    }

    /**
     * Determine whether the user can attach Permissions to Role.
     */
    public function attach(User $user, Role $role, array $permissions): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }
}
