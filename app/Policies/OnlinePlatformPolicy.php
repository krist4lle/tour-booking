<?php

namespace App\Policies;

use App\Models\OnlinePlatform;
use App\Models\User;
use App\Traits\GetPermissionName;

class OnlinePlatformPolicy
{
    use GetPermissionName;

    private string $className;

    public function __construct()
    {
        $this->className = OnlinePlatform::class;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, OnlinePlatform $onlinePlatform): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, OnlinePlatform $onlinePlatform): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, OnlinePlatform $onlinePlatform): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can attach OnlinePlatform to User.
     */
    public function attach(User $user, OnlinePlatform $onlinePlatform, User $model): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission) && $user->id === $model->id) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can detach OnlinePlatform from User.
     */
    public function detach(User $user, OnlinePlatform $onlinePlatform, User $model): bool
    {
        return $this->attach($user, $onlinePlatform, $model);
    }
}
