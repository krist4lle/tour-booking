<?php

namespace App\Policies;

use App\Models\User;
use App\Traits\GetPermissionName;

class UserPolicy
{
    use GetPermissionName;

    private string $className;

    public function __construct()
    {
        $this->className = User::class;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, User $model): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, User $model): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->id === $model->id) {

            return true;
        }

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, User $model): bool
    {
        $permission = $this->getPermissionName(__FUNCTION__, $this->className);

        if ($user->id === $model->id) {

            return true;
        }

        if ($user->hasPermissionTo($permission)) {

            return true;
        }

        return false;
    }
}
