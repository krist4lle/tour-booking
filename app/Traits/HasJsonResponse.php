<?php

namespace App\Traits;

trait HasJsonResponse
{
    public function response(mixed $data = null, string $message = 'OK', int $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'data' => $data,
            'message' => $message,
        ], $code);
    }
}
