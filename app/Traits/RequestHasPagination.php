<?php

namespace App\Traits;

use App\DTO\PaginationDTO;

trait RequestHasPagination
{
    public function createPaginationDTO(): PaginationDTO
    {
        return new PaginationDTO([
            'page' => $this->page ?? null,
            'perPage' => $this->per_page ?? null,
        ]);
    }
}
