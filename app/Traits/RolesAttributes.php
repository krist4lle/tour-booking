<?php

namespace App\Traits;

use App\Exceptions\UserException;
use Spatie\Permission\Models\Role;

trait RolesAttributes
{
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(self::ROLE_ADMIN);
    }

    public function isTrainer(): bool
    {
        return $this->hasRole(self::ROLE_TRAINER);
    }

    public function isStudent(): bool
    {
        return $this->hasRole(self::ROLE_STUDENT);
    }

    /**
     * @throws UserException
     */
    public function getRole(): Role
    {
        $this->checkRoles();

        return $this->roles()->first();
    }

    /**
     * @throws UserException
     */
    private function checkRoles(): void
    {
        if ($this->getRoleNames()->count() > 1) {
            throw UserException::hasMoreThanOneRole($this->id);
        }

        if ($this->getRoleNames()->count() < 1) {
            throw UserException::doesNotHaveRole($this->id);
        }
    }
}
