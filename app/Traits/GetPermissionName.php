<?php

namespace App\Traits;

trait GetPermissionName
{
    public function getPermissionName(string $action, string $className): string
    {
        return $action.' '.$className;
    }
}
