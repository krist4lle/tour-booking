<?php

namespace App\Traits;

use App\DTO\SortingDTO;

trait RequestHasSorting
{
    /**
     * @throws \Exception
     */
    public function createSortingDTO(): SortingDTO
    {
        return new SortingDTO([
            'column' => $this->column ?? null,
            'direction' => $this->direction ?? null,
        ]);
    }
}
