<?php

namespace App\QueryBuilders\SportQueryBuilder;

use App\QueryBuilders\Contract\AbstractQueryBuilder;
use Illuminate\Database\Eloquent\Builder;

class SportQueryBuilder extends AbstractQueryBuilder
{
    public function filterByUser(int $userId): Builder|SportQueryBuilder
    {
        return $this->whereHas('users', function (Builder $query) use ($userId) {
            $query->where('user_id', $userId);
        });
    }
}
