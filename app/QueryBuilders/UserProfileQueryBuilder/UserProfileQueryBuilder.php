<?php

namespace App\QueryBuilders\UserProfileQueryBuilder;

use App\QueryBuilders\Contract\AbstractQueryBuilder;

class UserProfileQueryBuilder extends AbstractQueryBuilder
{
    /**
     * @return $this
     */
    public function filterByName(?string $name): self
    {
        return $this->when(isset($name), function (self $query) use ($name) {
            $query
                ->where('user_profiles.first_name', 'LIKE', "%{$name}%")
                ->orWhere('user_profiles.last_name', 'LIKE', "%{$name}%");
        });
    }
}
