<?php

namespace App\QueryBuilders\Contract;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class AbstractQueryBuilder extends Builder implements QueryBuilderContract
{
    public function setOrder(SortingDTO $sortingDTO): self
    {
        return $this->orderBy($sortingDTO->getColumn(), $sortingDTO->getDirection());
    }

    public function setPagination(PaginationDTO $paginationDTO): LengthAwarePaginator
    {
        return $this->paginate(perPage: $paginationDTO->getPerPage(), page: $paginationDTO->getPage());
    }
}
