<?php

namespace App\QueryBuilders\Contract;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface QueryBuilderContract
{
    public function setOrder(SortingDTO $sortingDTO): self;

    public function setPagination(PaginationDTO $paginationDTO): LengthAwarePaginator;
}
