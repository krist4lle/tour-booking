<?php

namespace App\QueryBuilders\UserQueryBuilder;

use App\QueryBuilders\Contract\AbstractQueryBuilder;
use Illuminate\Database\Eloquent\Builder;

class UserQueryBuilder extends AbstractQueryBuilder
{
    public function selectFields(): self
    {
        return $this->select([
            'users.id',
            'users.email',
            'users.telegram_username',
            'users.created_at',
        ]);
    }

    public function joinProfile(): self
    {
        return $this->leftJoin('user_profiles as profile', 'users.id', '=', 'profile.user_id');
    }

    public function joinRoles(): self
    {
        return $this
            ->leftJoin('model_has_roles as mhr', 'users.id', '=', 'mhr.model_id')
            ->leftJoin('roles', 'mhr.role_id', '=', 'roles.id');
    }

    public function withoutAuthUser(): self
    {
        return $this->where('users.id', '!=', \Auth::id());
    }

    /**
     * @return $this
     */
    public function filterBySearchString(?string $search): self
    {
        return $this->when(isset($search), function (self $query) use ($search) {
            $query->where(function (self $query) use ($search) {
                $query
                    ->whereHas('profile', function (Builder $query) use ($search) {
                        $query
                            ->where('first_name', 'LIKE', "%{$search}%")
                            ->orWhere('last_name', 'LIKE', "%{$search}%")
                            ->orWhere('country', 'LIKE', "%{$search}%");
                    })
                    ->orWhere('email', 'LIKE', "%{$search}%");
            });
        });
    }

    /**
     * @return $this
     */
    public function filterByRole(?int $roleId): self
    {
        return $this->when(isset($roleId), function (self $query) use ($roleId) {
            $query->whereHas('roles', function (Builder $query) use ($roleId) {
                $query->where('role_id', $roleId);
            });
        });
    }

    /**
     * @return $this
     */
    public function filterByGender(?string $gender): self
    {
        return $this->when(isset($gender), function (self $query) use ($gender) {
            $query->whereHas('profile', fn (Builder $q) => $q->where('gender', $gender));
        });
    }
}
