<?php

namespace App\QueryBuilders\TrainingQueryBuilder;

use App\Models\User;
use App\QueryBuilders\Contract\AbstractQueryBuilder;
use App\QueryBuilders\UserProfileQueryBuilder\UserProfileQueryBuilder;
use App\QueryBuilders\UserQueryBuilder\UserQueryBuilder;
use Illuminate\Support\Carbon;

class TrainingQueryBuilder extends AbstractQueryBuilder
{
    public function selectFields(): self
    {
        return $this->select([
            'trainings.id',
            'trainings.name',
            'trainings.price',
            'trainings.online',
            'trainings.start_datetime',
            'trainings.duration',
            'trainings.created_at',

            'sports.name as sport_name',

            'user_profiles.first_name',
            'user_profiles.last_name',

            'training_types.name as training_type',
        ]);
    }

    /**
     * @return $this
     */
    public function filterBySearchString(?string $search): self
    {
        return $this->when(isset($search), function (self $query) use ($search) {
            $query->where(function (self $query) use ($search) {
                $query
                    ->where('trainings.name', 'LIKE', "%$search%")
                    ->orWhereHas('trainer', function (UserQueryBuilder $query) use ($search) {
                        $query->whereHas('profile', function (UserProfileQueryBuilder $query) use ($search) {
                            $query->filterByName($search);
                        });
                    });
            });
        });
    }

    /**
     * @return $this
     */
    public function filterBySport(?int $sportId): self
    {
        return $this->when(isset($sportId), fn (self $q) => $q->where('sport_id', $sportId));
    }

    /**
     * @return $this
     */
    public function filterByRole(): self
    {
        /** @var User $user */
        $user = auth()->user();

        return $this->when($user->isTrainer(), fn (self $q) => $q->where('trainer_id', $user->id));
    }

    /**
     * @return $this
     */
    public function filterByTrainingType(?int $trainingTypeId): self
    {
        return $this->when(isset($trainingTypeId), fn (self $q) => $q->where('training_type_id', $trainingTypeId));
    }

    /**
     * @return $this
     */
    public function filterByDate(?string $from, ?string $to): self
    {
        $from = isset($from) ? Carbon::make($from)->startOfDay() : null;
        $to = isset($to) ? Carbon::make($to)->endOfDay() : null;

        return $this
            ->when(isset($from), fn (self $q) => $q->where('start_datetime', '>=', $from))
            ->when(isset($to), fn (self $q) => $q->where('start_datetime', '<=', $to));
    }

    /**
     * @return $this
     */
    public function joinSport(): self
    {
        return $this->leftJoin('sports', 'trainings.sport_id', '=', 'sports.id');
    }

    /**
     * @return $this
     */
    public function joinTrainers(): self
    {
        return $this
            ->leftJoin('users', 'trainings.trainer_id', '=', 'users.id')
            ->leftJoin('user_profiles', 'users.id', '=', 'user_profiles.user_id');
    }

    /**
     * @return $this
     */
    public function joinTrainingType(): self
    {
        return $this->leftJoin('training_types', 'trainings.training_type_id', '=', 'training_types.id');
    }
}
