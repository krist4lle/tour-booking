<?php

namespace App\Services\Training;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Sport\SportDTO;
use App\DTO\Training\TrainingDTO;
use App\DTO\Training\TrainingFiltersDTO;
use App\Exceptions\SportException;
use App\Exceptions\TrainingException;
use App\Models\Sport;
use App\Models\Training;
use App\Repositories\Sport\SportRepositoryContract;
use App\Repositories\Training\TrainingRepositoryContract;
use App\Repositories\TrainingType\TrainingTypeRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TrainingService implements TrainingServiceContract
{
    public function __construct(
        private readonly SportRepositoryContract $sportRepository,
        private readonly TrainingTypeRepositoryContract $trainingTypeRepository,
        private readonly TrainingRepositoryContract $trainingRepository,
    ) {
    }

    public function getTrainings(
        TrainingFiltersDTO $trainingFiltersDTO,
        SortingDTO $sortingDTO,
        PaginationDTO $paginationDTO
    ): LengthAwarePaginator {
        return $this->trainingRepository->getEntities($trainingFiltersDTO, $sortingDTO, $paginationDTO);
    }

    public function getTrainingById(int $id): Training
    {
        $training = $this->trainingRepository->getById($id);

        if (empty($training)) {
            throw TrainingException::doesNotExist($id);
        }

        return $training;
    }

    public function storeTraining(TrainingDTO $trainingDTO): Training
    {
        return $this->trainingRepository->create($trainingDTO);
    }

    public function updateTraining(TrainingDTO $trainingDTO, Training $training): Training
    {
        return $this->trainingRepository->update($trainingDTO, $training);
    }

    public function deleteTraining(Training $training): void
    {
        $this->trainingRepository->delete($training);
    }

    public function getTrainingTypes(): Collection
    {
        return $this->trainingTypeRepository->getAll();
    }

    public function getSports(SortingDTO $sortingDTO, PaginationDTO $paginationDTO): LengthAwarePaginator
    {
        return $this->sportRepository->getEntities($sortingDTO, $paginationDTO);
    }

    public function getSportById(int $id): Sport
    {
        $sport = $this->sportRepository->getById($id);

        $this->doesSportExist($sport, $id);

        return $sport;
    }

    public function getSportByName(string $name): Sport
    {
        $sport = $this->sportRepository->getByName($name);

        $this->doesSportExist($sport, $name);

        return $sport;
    }

    private function doesSportExist(?Sport $sport, mixed $value): void
    {
        if (empty($sport)) {
            throw SportException::doesNotExist($value);
        }
    }

    public function storeSport(SportDTO $sportDTO): Sport
    {
        return $this->sportRepository->create($sportDTO);
    }

    public function updateSport(SportDTO $sportDTO, Sport $sport): Sport
    {
        return $this->sportRepository->update($sportDTO, $sport);
    }

    public function deleteSport(Sport $sport): void
    {
        $this->sportRepository->delete($sport);
    }
}
