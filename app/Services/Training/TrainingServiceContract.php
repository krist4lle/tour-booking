<?php

namespace App\Services\Training;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Sport\SportDTO;
use App\DTO\Training\TrainingDTO;
use App\DTO\Training\TrainingFiltersDTO;
use App\Models\Sport;
use App\Models\Training;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface TrainingServiceContract
{
    public function getTrainings(
        TrainingFiltersDTO $trainingFiltersDTO,
        SortingDTO $sortingDTO,
        PaginationDTO $paginationDTO
    ): LengthAwarePaginator;

    public function getTrainingById(int $id): Training;

    public function storeTraining(TrainingDTO $trainingDTO): Training;

    public function updateTraining(TrainingDTO $trainingDTO, Training $training): Training;

    public function deleteTraining(Training $training): void;

    public function getTrainingTypes(): Collection;

    public function getSports(SortingDTO $sortingDTO, PaginationDTO $paginationDTO): LengthAwarePaginator;

    public function getSportById(int $id): Sport;

    public function getSportByName(string $name): Sport;

    public function storeSport(SportDTO $sportDTO): Sport;

    public function updateSport(SportDTO $sportDTO, Sport $sport): Sport;

    public function deleteSport(Sport $sport): void;
}
