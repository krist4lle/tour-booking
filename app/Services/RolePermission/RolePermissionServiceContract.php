<?php

namespace App\Services\RolePermission;

use App\DTO\PaginationDTO;
use App\DTO\Permission\PermissionDTO;
use App\DTO\Permission\PermissionFiltersDTO;
use App\DTO\SortingDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

interface RolePermissionServiceContract
{
    public function getRoles(): Collection|LengthAwarePaginator;

    public function getRoleById(int $id): Role;

    public function getRoleByName(string $name): Role;

    public function syncPermissionsWithRole(Role $role, array $permissionsIds): void;

    public function getPermissions(
        PermissionFiltersDTO $permissionFiltersDTO,
        SortingDTO $sortingDTO,
        PaginationDTO $paginationDTO
    ): LengthAwarePaginator;

    public function storePermission(PermissionDTO $permissionDTO): Permission;

    public function updatePermission(PermissionDTO $permissionDTO, Permission $permission): Permission;

    public function deletePermission(Permission $permission): void;

    public function getDomains(): Collection;

    public function getActions(): array;
}
