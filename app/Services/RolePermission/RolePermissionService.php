<?php

namespace App\Services\RolePermission;

use App\DTO\Domain\DomainDTO;
use App\DTO\PaginationDTO;
use App\DTO\Permission\PermissionDTO;
use App\DTO\Permission\PermissionFiltersDTO;
use App\DTO\SortingDTO;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\Training;
use App\Models\User;
use App\Repositories\Permission\PermissionRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionService implements RolePermissionServiceContract
{
    private array $domains = [
        User::class,
        Sport::class,
        OnlinePlatform::class,
        Training::class,
        Permission::class,
    ];

    private array $actions = [
        'create',
        'update',
        'delete',
        'view',
        'viewAny',
        'attach',
        'detach',
    ];

    public function __construct(
        private readonly RoleRepositoryContract $roleRepository,
        private readonly PermissionRepositoryContract $permissionRepository
    ) {
    }

    public function getRoles(): Collection
    {
        return $this->roleRepository->getAll();
    }

    public function getRoleById(int $id): Role
    {
        /** @var Role|null $role */
        $role = $this->roleRepository->getById($id);

        return $role ?? throw RoleDoesNotExist::withId($id);
    }

    public function getRoleByName(string $name): Role
    {
        /** @var Role|null $role */
        $role = $this->roleRepository->getByName($name);

        return $role ?? throw RoleDoesNotExist::named($name);
    }

    public function syncPermissionsWithRole(Role $role, array $permissionsIds): void
    {
        $role->syncPermissions($permissionsIds);
    }

    public function getPermissions(
        PermissionFiltersDTO $permissionFiltersDTO,
        SortingDTO $sortingDTO,
        PaginationDTO $paginationDTO,
    ): LengthAwarePaginator {
        return $this->permissionRepository->getEntities($permissionFiltersDTO, $sortingDTO, $paginationDTO);
    }

    public function storePermission(PermissionDTO $permissionDTO): Permission
    {
        return $this->permissionRepository->create($permissionDTO);
    }

    public function updatePermission(PermissionDTO $permissionDTO, Permission $permission): Permission
    {
        return $this->permissionRepository->update($permissionDTO, $permission);
    }

    public function deletePermission(Permission $permission): void
    {
        $this->permissionRepository->delete($permission);
    }

    public function getDomains(): Collection
    {
        $domains = collect($this->domains);

        return $domains->map(fn (string $domain) => new DomainDTO($domain));
    }

    public function getActions(): array
    {
        return $this->actions;
    }
}
