<?php

namespace App\Services\User;

use App\DTO\OnlinePlatform\OnlinePlatformDTO;
use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\User\UserDTO;
use App\DTO\User\UserFiltersDTO;
use App\DTO\User\UserProfileDTO;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface UserServiceContract
{
    public function getUsers(UserFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator;

    public function getUserById(int $id): User;

    public function getUserByEmail(string $email): User;

    public function getUserByTelegramNickname(string $username): User;

    public function createUser(UserDTO $userDTO, UserProfileDTO $userProfileDTO): User;

    public function updateUser(UserDTO $userDTO, UserProfileDTO $userProfileDTO, User $user): User;

    public function deleteUser(User $user): void;

    public function updateUserPassword(string $password, User $user): User;

    public function attachOnlinePlatformToUser(User $user, OnlinePlatform $onlinePlatform, string $account): void;

    public function detachOnlinePlatformFromUser(User $user, OnlinePlatform $onlinePlatform): void;

    public function attachSportToUser(User $user, Sport $sport): void;

    public function detachSportFromUser(User $user, Sport $sport): void;

    public function attachUserToTraining(array $payload): void;

    public function detachUserFromTraining(array $payload): void;

    public function getOnlinePlatforms(SortingDTO $sortingDTO, PaginationDTO $paginationDTO): LengthAwarePaginator;

    public function getOnlinePlatformById(int $id): OnlinePlatform;

    public function getOnlinePlatformBySlug(string $slug): OnlinePlatform;

    public function storeOnlinePlatform(OnlinePlatformDTO $onlinePlatformDTO): OnlinePlatform;

    public function updateOnlinePlatform(OnlinePlatformDTO $onlinePlatformDTO, OnlinePlatform $onlinePlatform): OnlinePlatform;

    public function deleteOnlinePlatform(OnlinePlatform $onlinePlatform): void;
}
