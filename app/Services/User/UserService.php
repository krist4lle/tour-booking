<?php

namespace App\Services\User;

use App\DTO\OnlinePlatform\OnlinePlatformDTO;
use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\User\UserDTO;
use App\DTO\User\UserFiltersDTO;
use App\DTO\User\UserProfileDTO;
use App\Exceptions\OnlinePlatformException;
use App\Exceptions\TrainingException;
use App\Exceptions\UserException;
use App\Models\OnlinePlatform;
use App\Models\Sport;
use App\Models\User;
use App\Repositories\OnlinePlatform\OnlinePlatformRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserService implements UserServiceContract
{
    public function __construct(
        private readonly UserRepositoryContract $userRepository,
        private readonly OnlinePlatformRepositoryContract $onlinePlatformRepository
    ) {
    }

    public function getUsers(UserFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator
    {
        return $this->userRepository->getEntities($filters, $sort, $pagination);
    }

    public function getUserById(int $id): User
    {
        $user = $this->userRepository->getById($id);

        $this->doesUserExist($user, $id);

        return $user;
    }

    public function getUserByEmail(string $email): User
    {
        $user = $this->userRepository->getByEmail($email);

        $this->doesUserExist($user, $email);

        return $user;
    }

    public function getUserByTelegramNickname(string $username): User
    {
        $user = $this->userRepository->getByTelegramUsername($username);

        $this->doesUserExist($user, $username);

        return $user;
    }

    private function doesUserExist(?User $user, mixed $value): void
    {
        if (empty($user)) {
            throw UserException::doesNotExist($value);
        }
    }

    public function createUser(UserDTO $userDTO, UserProfileDTO $userProfileDTO): User
    {
        return $this->userRepository->createUser($userDTO, $userProfileDTO);
    }

    public function updateUser(UserDTO $userDTO, UserProfileDTO $userProfileDTO, User $user): User
    {
        return $this->userRepository->updateUser($user, $userDTO, $userProfileDTO);
    }

    public function deleteUser(User $user): void
    {
        $this->userRepository->deleteUser($user);
    }

    public function updateUserPassword(string $password, User $user): User
    {
        return $this->userRepository->updateUserPassword($password, $user);
    }

    public function attachOnlinePlatformToUser(User $user, OnlinePlatform $onlinePlatform, string $account): void
    {
        $user->onlinePlatforms()->syncWithPivotValues(
            $onlinePlatform,
            ['account' => $account],
            false
        );
    }

    public function detachOnlinePlatformFromUser(User $user, OnlinePlatform $onlinePlatform): void
    {
        $user->onlinePlatforms()->detach($onlinePlatform);
    }

    public function attachSportToUser(User $user, Sport $sport): void
    {
        if (! $user->isTrainer()) {
            throw UserException::notTrainer();
        }

        $user->sports()->attach($sport);
    }

    public function detachSportFromUser(User $user, Sport $sport): void
    {
        $user->sports()->detach($sport);
    }

    public function attachUserToTraining(array $payload): void
    {
        DB::beginTransaction();

        $training = $this->trainingService->getTraining($payload['training_id']);

        if ($training->isPersonalType() && $training->students->isNotEmpty()) {
            throw TrainingException::personalTrainingLimit();
        }

        $user = $this->getUser($payload['user_id']);

        if (! $user->isStudent()) {
            throw UserException::notStudent();
        }

        $training->students()->attach($payload['user_id']);

        DB::commit();
    }

    public function detachUserFromTraining(array $payload): void
    {
        DB::beginTransaction();

        $training = $this->trainingService->getTraining($payload['training_id']);
        $training->students()->detach($payload['user_id']);

        DB::commit();
    }

    public function getOnlinePlatforms(SortingDTO $sortingDTO, PaginationDTO $paginationDTO): LengthAwarePaginator
    {
        return $this->onlinePlatformRepository->getEntities($sortingDTO, $paginationDTO);
    }

    public function getOnlinePlatformById(int $id): OnlinePlatform
    {
        $onlinePlatform = $this->onlinePlatformRepository->getById($id);

        $this->doesOnlinePlatformExist($onlinePlatform, $id);

        return $onlinePlatform;
    }

    public function getOnlinePlatformBySlug(string $slug): OnlinePlatform
    {
        $onlinePlatform = $this->onlinePlatformRepository->getBySlug($slug);

        $this->doesOnlinePlatformExist($onlinePlatform, $slug);

        return $onlinePlatform;
    }

    private function doesOnlinePlatformExist(?OnlinePlatform $onlinePlatform, mixed $value): void
    {
        if (empty($onlinePlatform)) {
            throw OnlinePlatformException::doesNotExist($value);
        }
    }

    public function storeOnlinePlatform(OnlinePlatformDTO $onlinePlatformDTO): OnlinePlatform
    {
        return $this->onlinePlatformRepository->create($onlinePlatformDTO);
    }

    public function updateOnlinePlatform(OnlinePlatformDTO $onlinePlatformDTO, OnlinePlatform $onlinePlatform): OnlinePlatform
    {
        return $this->onlinePlatformRepository->update($onlinePlatformDTO, $onlinePlatform);
    }

    public function deleteOnlinePlatform(OnlinePlatform $onlinePlatform): void
    {
        $this->onlinePlatformRepository->delete($onlinePlatform);
    }

    private function getUserAsTrainer(int $userId): User
    {
        $user = $this->getUser($userId);

        if (! $user->isTrainer()) {
            throw UserException::notTrainer();
        }

        return $user;
    }
}
