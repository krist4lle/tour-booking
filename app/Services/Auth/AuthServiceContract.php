<?php

namespace App\Services\Auth;

use App\DTO\Auth\LoginDTO;
use App\DTO\User\UserDTO;
use App\Models\User;

interface AuthServiceContract
{
    public function registerUser(UserDTO $userDTO): User;

    public function loginUser(LoginDTO $loginDTO, string $type): bool;

    public function logout(): void;
}
