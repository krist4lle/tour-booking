<?php

namespace App\Services\Auth\LoginStrategy;

use App\DTO\Auth\LoginDTO;
use App\Repositories\User\UserRepositoryContract;

class LoginViaTelegramStrategy implements LoginStrategyContract
{
    public function login(LoginDTO $loginDTO): bool
    {
        /** @var UserRepositoryContract $userRepository */
        $userRepository = app(UserRepositoryContract::class);
        $user = $userRepository->getByTelegramToken($loginDTO->getTelegramToken());

        if (isset($user)) {
            \Auth::login($user, $loginDTO->isRemember());
        }

        return isset($user);
    }
}
