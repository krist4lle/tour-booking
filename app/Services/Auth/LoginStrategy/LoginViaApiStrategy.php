<?php

namespace App\Services\Auth\LoginStrategy;

use App\DTO\Auth\LoginDTO;
use App\Exceptions\AuthException;
use Illuminate\Support\Facades\Auth;

class LoginViaApiStrategy implements LoginStrategyContract
{
    public function login(LoginDTO $loginDTO): bool
    {
        $credentials = [
            'email' => $loginDTO->getEmail(),
            'password' => $loginDTO->getPassword(),
        ];

        $attempt = Auth::attempt($credentials, $loginDTO->isRemember());

        if (! $attempt) {
            throw AuthException::loginFailed();
        }

        return $attempt;
    }
}
