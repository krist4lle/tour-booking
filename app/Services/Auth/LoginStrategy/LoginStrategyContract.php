<?php

namespace App\Services\Auth\LoginStrategy;

use App\DTO\Auth\LoginDTO;

interface LoginStrategyContract
{
    public function login(LoginDTO $loginDTO): bool;
}
