<?php

namespace App\Services\Auth;

use App\DTO\Auth\LoginDTO;
use App\DTO\User\UserDTO;
use App\Exceptions\AuthException;
use App\Models\User;
use App\Repositories\User\UserRepositoryContract;
use App\Services\Auth\LoginStrategy\LoginStrategyContract;
use App\Services\Auth\LoginStrategy\LoginViaApiStrategy;
use App\Services\Auth\LoginStrategy\LoginViaTelegramStrategy;
use Illuminate\Support\Facades\Auth;

class AuthService implements AuthServiceContract
{
    private array $loginStrategies = [
        'api' => LoginViaApiStrategy::class,
        'telegram' => LoginViaTelegramStrategy::class,
    ];

    public function __construct(
        private readonly UserRepositoryContract $userRepository
    ) {
    }

    public function registerUser(UserDTO $userDTO): User
    {
        return $this->userRepository->createUser($userDTO);
    }

    public function loginUser(LoginDTO $loginDTO, string $type): bool
    {
        $strategy = $this->getLoginStrategy($type);

        return $strategy->login($loginDTO);
    }

    private function getLoginStrategy(string $type): LoginStrategyContract
    {
        if (! isset($this->loginStrategies[$type])) {
            throw AuthException::loginTypeNotSupported($type);
        }

        $className = $this->loginStrategies[$type];

        return new $className();
    }

    public function logout(): void
    {
        Auth::user()->tokens()->delete();
        Auth::guard('web')->logout();
    }
}
