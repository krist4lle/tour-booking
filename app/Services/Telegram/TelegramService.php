<?php

namespace App\Services\Telegram;

use App\DTO\Telegram\TelegramCallbackQueryDTO;
use App\DTO\Telegram\TelegramMessageDTO;
use App\DTO\Telegram\TelegramRequestDTO;
use App\Services\Telegram\Command\Contract\TelegramCommandContract;
use App\Services\Telegram\Command\TelegramRegisterCommand;
use App\Services\Telegram\Command\TelegramStartCommand;
use App\Services\User\UserServiceContract;

class TelegramService implements TelegramServiceContract
{
    private array $commands = [
        '/start' => TelegramStartCommand::class,
        '/register' => TelegramRegisterCommand::class,
    ];

    public function __construct(
        private readonly UserServiceContract $userService
    ) {
    }

    public function handleWebhook(TelegramRequestDTO $telegramRequestDTO): void
    {
        if ($telegramRequestDTO->getCallbackQuery() !== null) {
            $this->handleCallbackQuery($telegramRequestDTO->getCallbackQuery());
        }

        if ($telegramRequestDTO->getMessage() !== null) {
            $this->handleMessage($telegramRequestDTO->getMessage());
        }
    }

    public function handleMessage(TelegramMessageDTO $telegramMessageDTO): void
    {
        $command = $this->getCommand($telegramMessageDTO->getText());
        $command->execute($telegramMessageDTO->getChat()->getId(), $telegramMessageDTO);
    }

    public function handleCallbackQuery(TelegramCallbackQueryDTO $callbackQueryDTO): void
    {
        $queryData = json_decode($callbackQueryDTO->getData(), true);
        $command = $this->getCommand($queryData['action']);
        $command->execute($callbackQueryDTO->getMessage()->getChat()->getId(), $callbackQueryDTO);
    }

    private function getCommand(string $text): TelegramCommandContract
    {
        if (array_key_exists($text, $this->commands)) {
            $commandClass = $this->commands[$text];
            $command = new $commandClass();
        }

        if (! isset($command)) {
            $command = new TelegramStartCommand();
        }

        return $command;
    }
}
