<?php

namespace App\Services\Telegram\Command;

use App\DTO\Telegram\TelegramCallbackQueryDTO;
use App\DTO\Telegram\TelegramFromDTO;
use App\DTO\Telegram\TelegramMessageDTO;
use App\DTO\User\UserDTO;
use App\DTO\User\UserProfileDTO;
use App\Exceptions\TelegramException;
use App\Services\Telegram\Command\Contract\AbstractTelegramCommand;
use Illuminate\Support\Str;

class TelegramRegisterCommand extends AbstractTelegramCommand
{
    public function execute(int $chatId, TelegramMessageDTO|TelegramCallbackQueryDTO $DTO): void
    {
        if (! $DTO instanceof TelegramCallbackQueryDTO) {
            throw TelegramException::wrongDataType(TelegramCallbackQueryDTO::class);
        }

        $data = json_decode($DTO->getData(), true);
        $userDTO = $this->createUserDTO($DTO->getFrom(), (int) $data['roleId']);
        $userProfileDTO = $this->createUserProfileDTO($DTO->getFrom());

        $this->userService->createUser($userDTO, $userProfileDTO);

        $message = 'Congratulations, You have registered. Now You can use our app by menu pressing /start';

        $this->bot->sendMessage(chatId: $chatId, text: $message);
    }

    private function createUserDTO(TelegramFromDTO $from, int $roleId): UserDTO
    {
        return new UserDTO([
            'telegramUsername' => $from->getUsername(),
            'telegramToken' => $from->getId(),
            'roleId' => $roleId,
            'password' => Str::random(12),
        ]);
    }

    private function createUserProfileDTO(TelegramFromDTO $from): UserProfileDTO
    {
        return new UserProfileDTO([
            'firstName' => $from->getFirstName(),
            'lastName' => $from->getLastName(),
        ]);
    }
}
