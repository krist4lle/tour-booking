<?php

namespace App\Services\Telegram\Command;

use App\DTO\Auth\LoginDTO;
use App\DTO\Telegram\TelegramCallbackQueryDTO;
use App\DTO\Telegram\TelegramMessageDTO;
use App\Models\User;
use App\Services\Telegram\Command\Contract\AbstractTelegramCommand;

class TelegramStartCommand extends AbstractTelegramCommand
{
    public function execute(int $chatId, TelegramMessageDTO|TelegramCallbackQueryDTO $DTO): void
    {
        $loginDTO = $this->createLoginDTO($DTO->getFrom()->getId());
        $isLoggedIn = $this->authService->loginUser($loginDTO, 'telegram');

        $message = $this->createMessage($isLoggedIn);
        $keyboard = $this->createInlineKeyboardMarkup($this->createMarkup($isLoggedIn));

        $this->bot->sendMessage(chatId: $chatId, text: $message, replyMarkup: $keyboard);
    }

    private function createLoginDTO(int $telegramToken): LoginDTO
    {
        return new LoginDTO([
            'telegramToken' => $telegramToken,
        ]);
    }

    private function createMarkup(bool $isLoggedIn): array
    {
        $markup = [
            [
                [
                    'text' => 'Trainer',
                    'callback_data' => json_encode([
                        'roleId' => User::ROLE_TRAINER,
                        'action' => '/register',
                    ]),
                ],
                [
                    'text' => 'Student',
                    'callback_data' => json_encode([
                        'roleId' => User::ROLE_STUDENT,
                        'action' => '/register',
                    ]),
                ],
            ],
        ];

        if ($isLoggedIn) {
            $markup = [
                [
                    [
                        'text' => 'Sports',
                        'callback_data' => '/sports',
                    ],
                    [
                        'text' => 'Trainings',
                        'callback_data' => '/trainings',
                    ],
                    [
                        'text' => 'My profile',
                        'callback_data' => '/profile',
                    ],
                ],
            ];
        }

        return $markup;
    }

    private function createMessage(bool $isLoggedIn): string
    {
        $message = 'Welcome to our application. Please select you role.';

        if ($isLoggedIn) {
            $profile = \Auth::user()->profile;
            $message = "Hello {$profile->fullName}! Nice to see you.";
        }

        return $message;
    }
}
