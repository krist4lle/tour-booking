<?php

namespace App\Services\Telegram\Command\Contract;

use App\DTO\Telegram\TelegramCallbackQueryDTO;
use App\DTO\Telegram\TelegramMessageDTO;

interface TelegramCommandContract
{
    public function execute(int $chatId, TelegramMessageDTO|TelegramCallbackQueryDTO $DTO): void;
}
