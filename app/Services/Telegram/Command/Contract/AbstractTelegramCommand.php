<?php

namespace App\Services\Telegram\Command\Contract;

use App\Services\Auth\AuthServiceContract;
use App\Services\User\UserServiceContract;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

abstract class AbstractTelegramCommand implements TelegramCommandContract
{
    protected BotApi $bot;

    protected UserServiceContract $userService;

    protected AuthServiceContract $authService;

    public function __construct()
    {
        $this->bot = new BotApi(config('telegram.token'));
        $this->userService = app(UserServiceContract::class);
        $this->authService = app(AuthServiceContract::class);
    }

    protected function createInlineKeyboardMarkup(array $markup): InlineKeyboardMarkup
    {
        return new InlineKeyboardMarkup($markup);
    }
}
