<?php

namespace App\Services\Telegram;

use App\DTO\Telegram\TelegramRequestDTO;

interface TelegramServiceContract
{
    public function handleWebhook(TelegramRequestDTO $telegramRequestDTO): void;
}
