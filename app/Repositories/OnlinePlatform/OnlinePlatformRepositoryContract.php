<?php

namespace App\Repositories\OnlinePlatform;

use App\DTO\OnlinePlatform\OnlinePlatformDTO;
use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\Models\OnlinePlatform;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface OnlinePlatformRepositoryContract
{
    public function getAll(): Collection;

    public function getById(int $id): ?OnlinePlatform;

    public function getBySlug(string $slug): ?OnlinePlatform;

    public function getEntities(SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator;

    public function create(OnlinePlatformDTO $onlinePlatformDTO): OnlinePlatform;

    public function update(OnlinePlatformDTO $onlinePlatformDTO, OnlinePlatform $onlinePlatform): OnlinePlatform;

    public function delete(OnlinePlatform $onlinePlatform): void;
}
