<?php

namespace App\Repositories\OnlinePlatform;

use App\DTO\OnlinePlatform\OnlinePlatformDTO;
use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\Models\OnlinePlatform;
use App\QueryBuilders\OnlinePlatformQueryBuilder\OnlinePlatformQueryBuilder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OnlinePlatformRepository implements OnlinePlatformRepositoryContract
{
    protected OnlinePlatformQueryBuilder $query;

    public function __construct()
    {
        $this->query = OnlinePlatform::query();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getById(int $id): ?OnlinePlatform
    {
        return $this->query->find($id);
    }

    public function getBySlug(string $slug): ?OnlinePlatform
    {
        return $this->query->where('slug', $slug)->first();
    }

    public function getEntities(SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator
    {
        return $this->query
            ->withCount('users')
            ->setOrder($sort)
            ->setPagination($pagination);
    }

    public function create(OnlinePlatformDTO $onlinePlatformDTO): OnlinePlatform
    {
        DB::beginTransaction();
        $onlinePlatform = new OnlinePlatform();
        $onlinePlatform = $this->fill($onlinePlatformDTO, $onlinePlatform);
        $onlinePlatform->save();
        DB::commit();

        return $onlinePlatform;
    }

    public function update(OnlinePlatformDTO $onlinePlatformDTO, OnlinePlatform $onlinePlatform): OnlinePlatform
    {
        DB::beginTransaction();
        $onlinePlatform = $this->fill($onlinePlatformDTO, $onlinePlatform);
        $onlinePlatform->save();
        DB::commit();

        return $onlinePlatform;
    }

    public function delete(OnlinePlatform $onlinePlatform): void
    {
        DB::beginTransaction();
        $onlinePlatform->delete();
        DB::commit();
    }

    private function fill(OnlinePlatformDTO $onlinePlatformDTO, OnlinePlatform $onlinePlatform): OnlinePlatform
    {
        $onlinePlatform->name = $onlinePlatformDTO->getName();
        $onlinePlatform->slug = $onlinePlatformDTO->getSlug();

        return $onlinePlatform;
    }
}
