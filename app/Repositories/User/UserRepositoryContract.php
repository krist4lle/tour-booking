<?php

namespace App\Repositories\User;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\User\UserDTO;
use App\DTO\User\UserFiltersDTO;
use App\DTO\User\UserProfileDTO;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface UserRepositoryContract
{
    public function getAll(): Collection;

    public function getById(int $id): ?User;

    public function getByEmail(string $email): ?User;

    public function getByTelegramUsername(string $username): ?User;

    public function getByTelegramToken(int $token): ?User;

    public function getUsersByRole(int $roleId): Collection;

    public function getEntities(UserFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator;

    public function createUser(UserDTO $userDTO, ?UserProfileDTO $userProfileDTO = null): User;

    public function createProfile(?UserProfileDTO $userProfileDTO, User $user): UserProfile;

    public function updateUser(User $user, UserDTO $userDTO, ?UserProfileDTO $userProfileDTO = null): User;

    public function updateProfile(?UserProfileDTO $userProfileDTO, UserProfile $profile): UserProfile;

    public function updateUserPassword(string $password, User $user): User;

    public function deleteUser(User $user): void;
}
