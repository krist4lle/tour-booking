<?php

namespace App\Repositories\User;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\User\UserDTO;
use App\DTO\User\UserFiltersDTO;
use App\DTO\User\UserProfileDTO;
use App\Models\User;
use App\Models\UserProfile;
use App\QueryBuilders\UserQueryBuilder\UserQueryBuilder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryContract
{
    protected UserQueryBuilder $query;

    public function __construct()
    {
        $this->query = User::query();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getById(int $id): ?User
    {
        return $this->query->find($id);
    }

    public function getByEmail(string $email): ?User
    {
        return $this->query->where('email', $email)->first();
    }

    public function getByTelegramUsername(string $username): ?User
    {
        return $this->query->where('telegram_username', $username)->first();
    }

    public function getByTelegramToken(int $token): ?User
    {
        return $this->query->where('telegram_token', $token)->first();
    }

    public function getUsersByRole(int $roleId): Collection
    {
        return $this->query->filterByRole($roleId)->get();
    }

    public function getEntities(UserFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator
    {
        return $this->query
            ->selectFields()

            ->with([
                'profile',
                'roles.permissions',
                'onlinePlatforms',
            ])

            ->joinProfile()
            ->joinRoles()

            ->withoutAuthUser()

            ->filterBySearchString($filters->getSearch())
            ->filterByRole($filters->getRoleId())
            ->filterByGender($filters->getGender())

            ->setOrder($sort)
            ->setPagination($pagination);
    }

    public function createUser(UserDTO $userDTO, ?UserProfileDTO $userProfileDTO = null): User
    {
        DB::beginTransaction();
        $user = new User();

        if ($userDTO->getEmail() !== null) {
            $user->email = $userDTO->getEmail();
        }

        if ($userDTO->getTelegramUsername() !== null) {
            $user->telegram_username = $userDTO->getTelegramUsername();
            $user->telegram_token = $userDTO->getTelegramToken();
        }

        $user->password = $userDTO->getPassword();

        $user->save();
        $user->assignRole($userDTO->getRoleId());

        DB::commit();

        $this->createProfile($userProfileDTO, $user);

        return $user;
    }

    public function createProfile(?UserProfileDTO $userProfileDTO, User $user): UserProfile
    {
        DB::beginTransaction();
        $profile = new UserProfile();
        $profile = $this->fillProfile($userProfileDTO, $profile);
        $profile->user()->associate($user);
        $profile->save();
        DB::commit();

        return $profile;
    }

    public function updateUser(User $user, UserDTO $userDTO, ?UserProfileDTO $userProfileDTO = null): User
    {
        DB::beginTransaction();
        $user->email = $userDTO->getEmail();
        $user->save();

        if ($userDTO->getRoleId() && \Auth::user()->isSuperAdmin()) {
            $user->roles()->detach();
            $user->assignRole($userDTO->getRoleId());
        }
        DB::commit();

        $this->updateProfile($userProfileDTO, $user->profile);

        return $user;
    }

    public function updateProfile(?UserProfileDTO $userProfileDTO, UserProfile $profile): UserProfile
    {
        DB::beginTransaction();
        $profile = $this->fillProfile($userProfileDTO, $profile);
        $profile->save();
        DB::commit();

        return $profile;
    }

    public function updateUserPassword(string $password, User $user): User
    {
        DB::beginTransaction();
        $user->password = $password;
        $user->save();
        DB::commit();

        return $user;
    }

    private function fillProfile(?UserProfileDTO $userProfileDTO, UserProfile $profile): UserProfile
    {
        if (isset($userProfileDTO)) {
            $profile->first_name = $userProfileDTO->getFirstName();
            $profile->last_name = $userProfileDTO->getLastName();
            $profile->gender = $userProfileDTO->getGender();
            $profile->bio = $userProfileDTO->getBio();
            $profile->country = $userProfileDTO->getCountry();
            $profile->date_of_birth = $userProfileDTO->getDateOfBirth();
            $profile->avatar = $userProfileDTO->getAvatar();
        }

        return $profile;
    }

    public function deleteUser(User $user): void
    {
        DB::beginTransaction();
        $user->delete();
        DB::commit();
    }
}
