<?php

namespace App\Repositories\Sport;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Sport\SportDTO;
use App\Models\Sport;
use App\QueryBuilders\SportQueryBuilder\SportQueryBuilder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SportRepository implements SportRepositoryContract
{
    protected SportQueryBuilder $query;

    public function __construct()
    {
        $this->query = Sport::query();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getById(int $id): ?Sport
    {
        return $this->query->find($id);
    }

    public function getByName(string $name): ?Sport
    {
        return $this->query->where('name', $name)->first();
    }

    public function getEntities(SortingDTO $sort, PaginationDTO $paginate): LengthAwarePaginator
    {
        return $this->query
            ->withCount(['users', 'trainings'])
            ->setOrder($sort)
            ->setPagination($paginate);
    }

    public function create(SportDTO $sportDTO): Sport
    {
        DB::beginTransaction();
        $sport = new Sport();
        $sport->name = $sportDTO->getName();
        $sport->save();
        DB::commit();

        return $sport;
    }

    public function update(SportDTO $sportDTO, Sport $sport): Sport
    {
        DB::beginTransaction();
        $sport->name = $sportDTO->getName();
        $sport->save();
        DB::commit();

        return $sport;
    }

    public function delete(Sport $sport): void
    {
        DB::beginTransaction();
        $sport->delete();
        DB::commit();
    }
}
