<?php

namespace App\Repositories\Sport;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Sport\SportDTO;
use App\Models\Sport;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface SportRepositoryContract
{
    public function getAll(): Collection;

    public function getById(int $id): ?Sport;

    public function getByName(string $name): ?Sport;

    public function getEntities(SortingDTO $sort, PaginationDTO $paginate): LengthAwarePaginator;

    public function create(SportDTO $sportDTO): Sport;

    public function update(SportDTO $sportDTO, Sport $sport): Sport;

    public function delete(Sport $sport): void;
}
