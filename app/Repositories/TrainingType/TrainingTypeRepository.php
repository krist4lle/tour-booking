<?php

namespace App\Repositories\TrainingType;

use App\Models\TrainingType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class TrainingTypeRepository implements TrainingTypeRepositoryContract
{
    private Builder $query;

    public function __construct()
    {
        $this->query = TrainingType::query();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getById(int $id): ?TrainingType
    {
        return $this->query->find($id);
    }

    public function getBySlug(string $slug): ?TrainingType
    {
        return $this->query->where('slug', $slug)->first();
    }
}
