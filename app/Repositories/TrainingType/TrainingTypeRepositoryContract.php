<?php

namespace App\Repositories\TrainingType;

use App\Models\TrainingType;
use Illuminate\Support\Collection;

interface TrainingTypeRepositoryContract
{
    public function getAll(): Collection;

    public function getById(int $id): ?TrainingType;

    public function getBySlug(string $slug): ?TrainingType;
}
