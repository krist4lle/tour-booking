<?php

namespace App\Repositories\Training;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Training\TrainingDTO;
use App\DTO\Training\TrainingFiltersDTO;
use App\Exceptions\TrainingException;
use App\Models\Training;
use App\QueryBuilders\TrainingQueryBuilder\TrainingQueryBuilder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TrainingRepository implements TrainingRepositoryContract
{
    protected TrainingQueryBuilder $query;

    public function __construct()
    {
        $this->query = Training::query();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getById(int $id): ?Training
    {
        return $this->query->find($id);
    }

    public function getEntities(TrainingFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator
    {
        return $this->query
            ->selectFields()

            ->withCount('students')
            ->with([
                'trainer',
                'sport',
                'trainingType',
                'onlinePlatform',
            ])

            ->joinSport()
            ->joinTrainers()
            ->joinTrainingType()

            ->filterByRole()
            ->filterBySearchString($filters->getSearch())
            ->filterBySport($filters->getSportId())
            ->filterByTrainingType($filters->getTrainingTypeId())
            ->filterByDate($filters->getForm(), $filters->getTo())

            ->setOrder($sort)
            ->setPagination($pagination);
    }

    public function create(TrainingDTO $trainingDTO): Training
    {
        DB::beginTransaction();
        $training = new Training();
        $training = $this->fill($trainingDTO, $training);
        $training->save();
        DB::commit();

        return $training;
    }

    public function update(TrainingDTO $trainingDTO, Training $training): Training
    {
        DB::beginTransaction();
        $training = $this->fill($trainingDTO, $training);

        if ($training->isPersonalType() && $training->students->count() > 1) {
            throw TrainingException::personalTrainingLimit();
        }

        $training->save();
        DB::commit();

        return $training;
    }

    public function delete(Training $training): void
    {
        DB::beginTransaction();
        $training->delete();
        DB::commit();
    }

    private function fill(TrainingDTO $trainingDTO, Training $training): Training
    {
        if ($trainingDTO->isOnline() && $trainingDTO->getOnlinePlatformId() === null) {
            throw TrainingException::onlineTrainingDoesntHaveOnlinePlatform();
        }

        if (! $trainingDTO->isOnline() && $trainingDTO->getLocation() === null) {
            throw TrainingException::offlineTrainingDoesntHaveLocation();
        }

        $training->name = $trainingDTO->getName();
        $training->description = $trainingDTO->getDescription();
        $training->price = $trainingDTO->getPrice();
        $training->online = $trainingDTO->isOnline();
        $training->start_datetime = $trainingDTO->getStartDatetime();
        $training->duration = $trainingDTO->getDuration();
        $training->location = $trainingDTO->getLocation();

        $training->trainingType()->associate($trainingDTO->getTrainingTypeId());
        $training->trainer()->associate($trainingDTO->getTrainerId());
        $training->sport()->associate($trainingDTO->getSportId());
        $training->onlinePlatform()->associate($trainingDTO->getOnlinePlatformId());

        return $training;
    }
}
