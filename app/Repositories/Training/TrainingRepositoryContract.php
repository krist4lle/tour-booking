<?php

namespace App\Repositories\Training;

use App\DTO\PaginationDTO;
use App\DTO\SortingDTO;
use App\DTO\Training\TrainingDTO;
use App\DTO\Training\TrainingFiltersDTO;
use App\Models\Training;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface TrainingRepositoryContract
{
    public function getAll(): Collection;

    public function getById(int $id): ?Training;

    public function getEntities(TrainingFiltersDTO $filters, SortingDTO $sort, PaginationDTO $pagination): LengthAwarePaginator;

    public function create(TrainingDTO $trainingDTO): Training;

    public function update(TrainingDTO $trainingDTO, Training $training): Training;

    public function delete(Training $training): void;
}
