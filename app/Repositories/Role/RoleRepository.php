<?php

namespace App\Repositories\Role;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;

class RoleRepository implements RoleRepositoryContract
{
    private Builder $query;

    public function __construct()
    {
        $this->query = Role::query();
    }

    public function getById(int $id): Role|Model|null
    {
        return $this->query->find($id);
    }

    public function getByName(string $name): Role|Model|null
    {
        return $this->query->where('name', $name)->first();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getWithoutAdmin(): Collection
    {
        return $this->query->where('name', '!=', 'admin')->get();
    }
}
