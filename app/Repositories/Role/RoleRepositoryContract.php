<?php

namespace App\Repositories\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;

interface RoleRepositoryContract
{
    public function getById(int $id): Role|Model|null;

    public function getByName(string $name): Role|Model|null;

    public function getAll(): Collection;

    public function getWithoutAdmin(): Collection;
}
