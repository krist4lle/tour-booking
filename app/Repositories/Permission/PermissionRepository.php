<?php

namespace App\Repositories\Permission;

use App\DTO\PaginationDTO;
use App\DTO\Permission\PermissionDTO;
use App\DTO\Permission\PermissionFiltersDTO;
use App\DTO\SortingDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Exceptions\PermissionAlreadyExists;
use Spatie\Permission\Models\Permission;

class PermissionRepository implements PermissionRepositoryContract
{
    private Builder $query;

    private string $guardName = 'web';

    public function __construct()
    {
        $this->query = Permission::query();
    }

    public function getById(int $id): null|Model|Permission
    {
        return $this->query->find($id);
    }

    public function getByName(string $name): null|Model|Permission
    {
        return $this->query->where('name', $name)->first();
    }

    public function getAll(): Collection
    {
        return $this->query->get();
    }

    public function getEntities(
        PermissionFiltersDTO $filters,
        SortingDTO $sort,
        PaginationDTO $pagination
    ): LengthAwarePaginator {
        return $this->query
            ->when(
                $filters->getSearch(),
                fn (Builder $query) => $query->where('name', 'LIKE', "%{$filters->getSearch()}%")
            )
            ->when(
                $filters->getRoleId(),
                function (Builder $query) use ($filters) {
                    $query->whereHas('roles', function (Builder $query) use ($filters) {
                        $query->where('role_id', $filters->getRoleId());
                    });
                }
            )
            ->orderBy($sort->getColumn(), $sort->getDirection())
            ->paginate(perPage: $pagination->getPerPage(), page: $pagination->getPage());
    }

    public function create(PermissionDTO $permissionDTO): Permission
    {
        DB::beginTransaction();

        $permission = new Permission();
        $permission = $this->fill($permissionDTO, $permission);
        $permission->save();

        DB::commit();

        return $permission;
    }

    public function update(PermissionDTO $permissionDTO, Permission $permission): Permission
    {
        DB::beginTransaction();

        $permission = $this->fill($permissionDTO, $permission);
        $permission->save();

        DB::commit();

        return $permission;
    }

    private function fill(PermissionDTO $permissionDTO, Permission $permission): Permission
    {
        if ($this->query->where('name', $permissionDTO->getName())->exists()) {
            throw PermissionAlreadyExists::create($permissionDTO->getName(), $this->guardName);
        }

        $permission->name = $permissionDTO->getName();
        $permission->guard_name = $this->guardName;

        return $permission;
    }

    public function delete(Permission $permission): void
    {
        DB::beginTransaction();
        $permission->delete();
        DB::commit();
    }
}
