<?php

namespace App\Repositories\Permission;

use App\DTO\PaginationDTO;
use App\DTO\Permission\PermissionDTO;
use App\DTO\Permission\PermissionFiltersDTO;
use App\DTO\SortingDTO;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;

interface PermissionRepositoryContract
{
    public function getById(int $id): null|Model|Permission;

    public function getByName(string $name): null|Model|Permission;

    public function getAll(): Collection;

    public function getEntities(
        PermissionFiltersDTO $filters,
        SortingDTO $sort,
        PaginationDTO $pagination
    ): LengthAwarePaginator;

    public function create(PermissionDTO $permissionDTO): Permission;

    public function update(PermissionDTO $permissionDTO, Permission $permission): Permission;

    public function delete(Permission $permission): void;
}
