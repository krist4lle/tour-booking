<?php

namespace App\Exceptions;

class UserException extends CustomException
{
    public static function hasMoreThanOneRole(int $id): self
    {
        return new self("User can not have more than one role. Please contact Admin. User ID <{$id}>", 400);
    }

    public static function doesNotHaveRole(int $id): self
    {
        return new self("User does not have role. Please contact Admin. User ID <{$id}>", 400);
    }

    public static function doesNotExist(int|string $value): self
    {
        return new self("User <{$value}> does not exist. Check your data.", 400);
    }

    public static function notTrainer(): self
    {
        return new self('User is not a Trainer.', 403);
    }

    public static function notStudent(): self
    {
        return new self('User is not a Student.', 403);
    }
}
