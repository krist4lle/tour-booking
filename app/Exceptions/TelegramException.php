<?php

namespace App\Exceptions;

class TelegramException extends CustomException
{
    public static function wrongDataType(string $value): self
    {
        return new self("You have provided wrong data type. Please check your data. {$value} must be provided.", 400);
    }
}
