<?php

namespace App\Exceptions;

class SportException extends CustomException
{
    public static function doesNotExist(int $id): self
    {
        return new self("Sport with ID <{$id}> does not exist. Contact with Admin.", 404);
    }
}
