<?php

namespace App\Exceptions;

class TrainingException extends CustomException
{
    public static function personalTrainingLimit(): self
    {
        return new self('You can not attach more than one User to Personal Training.', 400);
    }

    public static function doesNotExist(int $id): self
    {
        return new self("Training with ID <{$id}> does not exist. Contact with Admin.", 404);
    }

    public static function onlineTrainingDoesntHaveOnlinePlatform(): self
    {
        return new self('Online Training must have an Online Platform.', 400);
    }

    public static function offlineTrainingDoesntHaveLocation(): self
    {
        return new self('Offline Training must have a Location.', 400);
    }
}
