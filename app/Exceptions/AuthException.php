<?php

namespace App\Exceptions;

class AuthException extends CustomException
{
    public static function loginTypeNotSupported(string $type): self
    {
        return new self("Login type <{$type}> is not supported.", 400);
    }

    public static function loginFailed(): self
    {
        return new self('Login attempt failed. Please contact Admin.', 400);
    }
}
