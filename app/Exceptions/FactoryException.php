<?php

namespace App\Exceptions;

class FactoryException extends CustomException
{
    public static function wrongData(?string $key = null): self
    {
        return new self("Please check incoming data. <{$key}> is missed", 406);
    }

    public static function alreadyExists(): self
    {
        return new self('This Model already exists. Check your data.', 406);
    }
}
