<?php

namespace App\Exceptions;

class RepositoryException extends CustomException
{
    public static function wrongData(?string $key = null): self
    {
        return new self("Please check incoming data. <{$key}> is missed", 406);
    }
}
