<?php

namespace App\Exceptions;

class OnlinePlatformException extends CustomException
{
    public static function doesNotExist(int $id): self
    {
        return new self("Online platform with ID <{$id}> does not exist. Contact with Admin.", 404);
    }
}
