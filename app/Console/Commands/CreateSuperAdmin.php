<?php

namespace App\Console\Commands;

use App\DTO\User\UserDTO;
use App\Models\User;
use App\Models\UserProfile;
use App\Services\User\UserServiceContract;
use Illuminate\Console\Command;

class CreateSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-super-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create default Super Admin.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var User $user */
        $user = User::updateOrCreate(
            ['email' => 'admin@email.com'],
            ['password' => 'password']
        );

        $user->assignRole('admin');

        if (empty($user->profile)) {
            UserProfile::create([
                'user_id' => $user->id,
                'first_name' => 'Super',
                'last_name' => 'Admin'
            ]);
        }

        $this->info('Super admin has been created. Email: <admin@email.com>; Password: <password>');
    }
}
