<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Console\Command;

class DropUsers extends Command
{
    private UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();

        $this->userRepository = new UserRepository(User::class);
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:drop-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop Users from database.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $type = $this->choice('Roles to drop', ['trainers', 'students']);

        switch ($type) {
            case 'trainers':
                $users = $this->userRepository->getTrainers();
                break;
            case 'students':
                $users = $this->userRepository->getStudents();
                break;
        }

        $users->each(fn (User $user) => $user->delete());
    }
}
