<?php

namespace App\Console\Commands;

use App\Models\Training;
use App\Repositories\Training\TrainingRepository;
use Illuminate\Console\Command;

class DropTrainings extends Command
{
    private TrainingRepository $trainingRepository;

    public function __construct()
    {
        parent::__construct();

        $this->trainingRepository = new TrainingRepository(Training::class);
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:drop-trainings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop Trainings from database.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->trainingRepository->getAll()->each(fn (Training $training) => $training->delete());
    }
}
