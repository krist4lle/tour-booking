<?php

namespace App\DTO\Telegram;

final class TelegramRequestDTO
{
    private ?TelegramMessageDTO $message;

    private ?TelegramCallbackQueryDTO $callbackQuery;

    public function __construct(array $data)
    {
        $this->message = isset($data['message']) ? new TelegramMessageDTO($data['message']) : null;
        $this->callbackQuery = isset($data['callback_query']) ? new TelegramCallbackQueryDTO($data['callback_query']) : null;
    }

    public function getMessage(): ?TelegramMessageDTO
    {
        return $this->message;
    }

    public function getCallbackQuery(): ?TelegramCallbackQueryDTO
    {
        return $this->callbackQuery;
    }
}
