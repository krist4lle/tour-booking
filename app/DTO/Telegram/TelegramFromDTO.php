<?php

namespace App\DTO\Telegram;

final class TelegramFromDTO
{
    private int $id;

    private ?string $firstName;

    private ?string $lastName;

    private ?string $languageCode;

    private ?string $username;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->firstName = $data['first_name'] ?? null;
        $this->lastName = $data['last_name'] ?? null;
        $this->languageCode = $data['language_code'] ?? null;
        $this->username = $data['username'] ?? null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }
}
