<?php

namespace App\DTO\Telegram;

final class TelegramCallbackQueryDTO
{
    private string $data;

    private TelegramFromDTO $from;

    private TelegramMessageDTO $message;

    public function __construct(array $data)
    {
        $this->data = $data['data'];
        $this->from = new TelegramFromDTO($data['from']);
        $this->message = new TelegramMessageDTO($data['message']);
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function getFrom(): TelegramFromDTO
    {
        return $this->from;
    }

    public function getMessage(): TelegramMessageDTO
    {
        return $this->message;
    }
}
