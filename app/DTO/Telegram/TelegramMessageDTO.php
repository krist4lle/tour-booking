<?php

namespace App\DTO\Telegram;

final class TelegramMessageDTO
{
    private TelegramFromDTO $from;

    private TelegramChatDTO $chat;

    private string $text;

    public function __construct(array $data)
    {
        $this->from = new TelegramFromDTO($data['from']);
        $this->chat = new TelegramChatDTO($data['chat']);
        $this->text = $data['text'];
    }

    public function getFrom(): TelegramFromDTO
    {
        return $this->from;
    }

    public function getChat(): TelegramChatDTO
    {
        return $this->chat;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
