<?php

namespace App\DTO\User;

final class UserDTO
{
    private ?string $email;

    private ?string $password;

    private ?int $roleId;

    private ?string $telegramUsername;

    private ?string $telegramToken;

    public function __construct(array $data)
    {
        $this->email = $data['email'] ?? null;
        $this->password = $data['password'] ?? null;
        $this->roleId = $data['roleId'] ?? null;
        $this->telegramUsername = $data['telegramUsername'] ?? null;
        $this->telegramToken = $data['telegramToken'] ?? null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRoleId(): ?int
    {
        return $this->roleId;
    }

    public function getTelegramUsername(): ?string
    {
        return $this->telegramUsername;
    }

    public function getTelegramToken(): ?string
    {
        return $this->telegramToken;
    }
}
