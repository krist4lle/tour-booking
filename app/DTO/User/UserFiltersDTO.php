<?php

namespace App\DTO\User;

final class UserFiltersDTO
{
    private ?string $search;

    private ?int $roleId;

    private ?string $gender;

    public function __construct(array $data)
    {
        $this->search = $data['search'] ?? null;
        $this->roleId = $data['roleId'] ?? null;
        $this->gender = $data['gender'] ?? null;
    }

    public function getSearch(): ?string
    {
        return $this->search;
    }

    public function getRoleId(): ?int
    {
        return $this->roleId;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }
}
