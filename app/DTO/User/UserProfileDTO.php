<?php

namespace App\DTO\User;

use Illuminate\Http\UploadedFile;

final class UserProfileDTO
{
    private ?string $firstName;

    private ?string $lastName;

    private ?string $gender;

    private ?string $bio;

    private ?string $dateOfBirth;

    private ?string $country;

    private ?UploadedFile $avatar;

    private array $properties = [
        'firstName',
        'lastName',
        'gender',
        'bio',
        'dateOfBirth',
        'country',
        'avatar',
    ];

    public function __construct(array $data)
    {
        foreach ($this->properties as $property) {
            $this->$property = $data[$property] ?? null;
        }
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getAvatar(): ?UploadedFile
    {
        return $this->avatar;
    }
}
