<?php

namespace App\DTO;

final class SortingDTO
{
    private string $column;

    private string $direction;

    private array $directions = [
        'asc',
        'desc',
    ];

    public function __construct(array $data)
    {
        if (isset($data['direction']) && ! in_array($data['direction'], $this->directions)) {
            throw new \Exception('Direction must be a value of <asc> or <desc>.');
        }

        $this->column = $data['column'] ?? 'created_at';
        $this->direction = $data['direction'] ?? 'asc';
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }
}
