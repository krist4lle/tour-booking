<?php

namespace App\DTO\Permission;

use App\Exceptions\RepositoryException;

final class PermissionDTO
{
    private string $domain;

    private string $action;

    public function __construct(array $data)
    {
        if (empty($data['domain'])) {
            throw RepositoryException::wrongData('domain');
        }

        if (empty($data['action'])) {
            throw RepositoryException::wrongData('action');
        }

        $this->domain = $data['domain'];
        $this->action = $data['action'];
    }

    public function getName(): string
    {
        return "{$this->action} {$this->domain}";
    }
}
