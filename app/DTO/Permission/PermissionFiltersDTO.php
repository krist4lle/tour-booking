<?php

namespace App\DTO\Permission;

final class PermissionFiltersDTO
{
    private ?string $search;

    private ?int $roleId;

    public function __construct(array $data)
    {
        $this->search = $data['search'] ?? null;
        $this->roleId = $data['role_id'] ?? null;
    }

    public function getSearch(): ?string
    {
        return $this->search;
    }

    public function getRoleId(): ?int
    {
        return $this->roleId;
    }
}
