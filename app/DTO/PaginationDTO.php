<?php

namespace App\DTO;

final class PaginationDTO
{
    private int $page;

    private int $perPage;

    public function __construct(array $data)
    {
        $this->page = $data['page'] ?? 1;
        $this->perPage = $data['perPage'] ?? 10;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }
}
