<?php

namespace App\DTO\Training;

final class TrainingFiltersDTO
{
    private ?string $search;

    private ?int $sportId;

    private ?int $trainerId;

    private ?int $trainingTypeId;

    private ?string $form;

    private ?string $to;

    private array $properties = [
        'search',
        'sportId',
        'trainerId',
        'trainingTypeId',
        'from',
        'to',
    ];

    public function __construct(array $data)
    {
        foreach ($this->properties as $property) {
            if (empty($data[$property])) {
                $this->$property = null;
            }

            $this->$property = $data[$property];
        }
    }

    public function getSearch(): ?string
    {
        return $this->search;
    }

    public function getSportId(): ?int
    {
        return $this->sportId;
    }

    public function getTrainerId(): ?int
    {
        return $this->trainerId;
    }

    public function getTrainingTypeId(): ?int
    {
        return $this->trainingTypeId;
    }

    public function getForm(): ?string
    {
        return $this->form;
    }

    public function getTo(): ?string
    {
        return $this->to;
    }
}
