<?php

namespace App\DTO\Training;

use App\Exceptions\RepositoryException;

final class TrainingDTO
{
    private string $name;

    private ?string $description;

    private ?float $price;

    private bool $online;

    private int $trainingTypeId;

    private int $sportId;

    private int $trainerId;

    private string $startDatetime;

    private int $duration;

    private ?string $location;

    private ?int $onlinePlatformId;

    private array $properties = [
        'name',
        'description',
        'price',
        'online',
        'trainingTypeId',
        'sportId',
        'trainerId',
        'startDatetime',
        'duration',
        'location',
        'onlinePlatformId',
    ];

    public function __construct(array $data)
    {
        foreach ($this->properties as $property) {
            if (empty($data[$property])) {
                throw RepositoryException::wrongData($property);
            }

            $this->$property = $data[$property];
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function isOnline(): bool
    {
        return $this->online;
    }

    public function getTrainingTypeId(): int
    {
        return $this->trainingTypeId;
    }

    public function getSportId(): int
    {
        return $this->sportId;
    }

    public function getTrainerId(): int
    {
        return $this->trainerId;
    }

    public function getStartDatetime(): string
    {
        return $this->startDatetime;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getOnlinePlatformId(): ?int
    {
        return $this->onlinePlatformId;
    }
}
