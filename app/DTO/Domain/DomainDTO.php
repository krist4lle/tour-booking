<?php

namespace App\DTO\Domain;

final class DomainDTO
{
    private string $name;

    private string $class;

    public function __construct(string $class)
    {
        if (! class_exists($class)) {
            throw new \Exception("Class {$class} does not exist.");
        }

        $this->name = class_basename($class);
        $this->class = $class;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClass(): string
    {
        return $this->class;
    }
}
