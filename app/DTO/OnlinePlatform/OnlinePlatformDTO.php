<?php

namespace App\DTO\OnlinePlatform;

use Illuminate\Support\Str;

final class OnlinePlatformDTO
{
    private string $name;

    private string $slug;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->slug = Str::slug($name);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}
