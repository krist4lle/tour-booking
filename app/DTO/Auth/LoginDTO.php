<?php

namespace App\DTO\Auth;

final class LoginDTO
{
    private ?string $email;

    private ?string $password;

    private ?int $telegramToken;

    private bool $remember;

    public function __construct(array $data)
    {
        $this->email = $data['email'] ?? null;
        $this->password = $data['password'] ?? null;
        $this->telegramToken = $data['telegramToken'] ?? null;
        $this->remember = $data['remember'] ?? false;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getTelegramToken(): ?int
    {
        return $this->telegramToken;
    }

    public function isRemember(): bool
    {
        return $this->remember;
    }
}
