<?php

namespace App\Models;

use App\QueryBuilders\UserProfileQueryBuilder\UserProfileQueryBuilder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserProfile
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $fullName
 * @property string|null $gender
 * @property string|null $fullGender
 * @property string|null $bio
 * @property string|null $date_of_birth
 * @property string|null $country
 * @property string|null $avatar
 * @property User $user
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static UserProfileQueryBuilder|UserProfile filterByName(?string $name)
 * @method static UserProfileQueryBuilder|UserProfile newModelQuery()
 * @method static UserProfileQueryBuilder|UserProfile newQuery()
 * @method static UserProfileQueryBuilder|UserProfile query()
 * @method static UserProfileQueryBuilder|UserProfile setOrder(array $sort = [])
 * @method static UserProfileQueryBuilder|UserProfile setPagination(array $meta = [])
 * @method static UserProfileQueryBuilder|UserProfile whereAvatar($value)
 * @method static UserProfileQueryBuilder|UserProfile whereBio($value)
 * @method static UserProfileQueryBuilder|UserProfile whereCountry($value)
 * @method static UserProfileQueryBuilder|UserProfile whereCreatedAt($value)
 * @method static UserProfileQueryBuilder|UserProfile whereDateOfBirth($value)
 * @method static UserProfileQueryBuilder|UserProfile whereFirstName($value)
 * @method static UserProfileQueryBuilder|UserProfile whereGender($value)
 * @method static UserProfileQueryBuilder|UserProfile whereId($value)
 * @method static UserProfileQueryBuilder|UserProfile whereLastName($value)
 * @method static UserProfileQueryBuilder|UserProfile whereUpdatedAt($value)
 * @method static UserProfileQueryBuilder|UserProfile whereUserId($value)
 *
 * @mixin \Eloquent
 */
class UserProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'gender',
        'bio',
        'date_of_birth',
        'country',
        'avatar',
    ];

    protected $casts = [
        'date_of_birth' => 'date:d.m.Y',
    ];

    public const MALE = 'M';

    public const FEMALE = 'F';

    public function newEloquentBuilder($query): UserProfileQueryBuilder
    {
        return new UserProfileQueryBuilder($query);
    }

    /************
     * Relations
     ************/

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /************
     * Attributes
     ************/

    protected static function dateOfBirth(): Attribute
    {
        return Attribute::make(
            set: fn (string|null $dateOfBirth) => isset($dateOfBirth) ? date_create($dateOfBirth) : null
        );
    }

    protected static function fullName(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                if (isset($attributes['first_name']) && isset($attributes['last_name'])) {

                    return $attributes['first_name'].' '.$attributes['last_name'];
                }

                return null;
            }
        );
    }

    protected static function fullGender(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                if (isset($attributes['gender'])) {

                    return $attributes['gender'] === self::MALE ? 'Male' : 'Female';
                }

                return null;
            }
        );
    }
}
