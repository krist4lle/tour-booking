<?php

namespace App\Models;

use App\QueryBuilders\OnlinePlatformQueryBuilder\OnlinePlatformQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\OnlinePlatform
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property Training[]|Collection $trainings
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read int|null $trainings_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, User> $users
 * @property-read int|null $users_count
 *
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform newModelQuery()
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform newQuery()
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform query()
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform setOrder(array $sort = [])
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform setPagination(array $meta = [])
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform whereCreatedAt($value)
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform whereId($value)
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform whereName($value)
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform whereSlug($value)
 * @method static OnlinePlatformQueryBuilder|OnlinePlatform whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class OnlinePlatform extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
    ];

    public const SORTABLE_COLUMNS = [
        'name',
        'created_at',
        'users_count',
    ];

    public function newEloquentBuilder($query): OnlinePlatformQueryBuilder
    {
        return new OnlinePlatformQueryBuilder($query);
    }

    /************
     * Relations
     ************/

    public function trainings(): HasMany
    {
        return $this->hasMany(Training::class, 'online_platform_id', 'id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_online_platform',
            'online_platform_id',
            'user_id'
        )
            ->withPivot('account');
    }
}
