<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\TrainingType
 *
 * @property $id
 * @property $name
 * @property $slug
 * @property $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection<int, Training> $trainings
 * @property-read int|null $trainings_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrainingType whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class TrainingType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public const GROUP = 'group';

    public const PERSONAL = 'personal';

    /************
     * Relations
     ************/

    public function trainings(): HasMany
    {
        return $this->hasMany(Training::class, 'training_type_id', 'id');
    }

    /************
     * Attributes
     ************/

    public function isGroup(): bool
    {
        return $this->slug === $this::GROUP;
    }

    public function isPersonal(): bool
    {
        return $this->slug === $this::PERSONAL;
    }
}
