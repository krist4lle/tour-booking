<?php

namespace App\Models;

use App\QueryBuilders\TrainingQueryBuilder\TrainingQueryBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Training
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $price
 * @property bool $online
 * @property \DateTime $start_datetime
 * @property int $duration
 * @property string $location
 * @property int $training_type_id
 * @property int $sport_id
 * @property int $trainer_id
 * @property int $online_platform_id
 * @property OnlinePlatform $onlinePlatform
 * @property User $trainer
 * @property Sport $sport
 * @property TrainingType $trainingType
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection<int, User> $students
 * @property-read int|null $students_count
 *
 * @method static TrainingQueryBuilder|Training filterByDate(?string $from, ?string $to)
 * @method static TrainingQueryBuilder|Training filterBySearchString(?string $search)
 * @method static TrainingQueryBuilder|Training filterBySport(?int $sportId)
 * @method static TrainingQueryBuilder|Training filterByTrainer(?int $trainerId)
 * @method static TrainingQueryBuilder|Training filterByTrainingType(?int $trainingTypeId)
 * @method static TrainingQueryBuilder|Training joinSport()
 * @method static TrainingQueryBuilder|Training joinTrainers()
 * @method static TrainingQueryBuilder|Training joinTrainingType()
 * @method static TrainingQueryBuilder|Training newModelQuery()
 * @method static TrainingQueryBuilder|Training newQuery()
 * @method static TrainingQueryBuilder|Training query()
 * @method static TrainingQueryBuilder|Training selectIndex()
 * @method static TrainingQueryBuilder|Training setOrder(array $sort = [])
 * @method static TrainingQueryBuilder|Training setPagination(array $meta = [])
 * @method static TrainingQueryBuilder|Training whereCreatedAt($value)
 * @method static TrainingQueryBuilder|Training whereDescription($value)
 * @method static TrainingQueryBuilder|Training whereDuration($value)
 * @method static TrainingQueryBuilder|Training whereId($value)
 * @method static TrainingQueryBuilder|Training whereLocation($value)
 * @method static TrainingQueryBuilder|Training whereName($value)
 * @method static TrainingQueryBuilder|Training whereOnline($value)
 * @method static TrainingQueryBuilder|Training whereOnlinePlatformId($value)
 * @method static TrainingQueryBuilder|Training wherePrice($value)
 * @method static TrainingQueryBuilder|Training whereSportId($value)
 * @method static TrainingQueryBuilder|Training whereStartDatetime($value)
 * @method static TrainingQueryBuilder|Training whereTrainerId($value)
 * @method static TrainingQueryBuilder|Training whereTrainingTypeId($value)
 * @method static TrainingQueryBuilder|Training whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Training extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'online',
        'start_datetime',
        'duration',
        'location',
    ];

    protected $casts = [
        'start_datetime' => 'datetime',
        'online' => 'boolean',
        'price' => 'decimal:2',
    ];

    public const SORTABLE_COLUMNS = [
        'name',
        'price',
        'online',
        'start_datetime',
        'duration',
        'location',
        'created_at',
        'sport_name',
        'first_name',
        'training_type',
    ];

    public function newEloquentBuilder($query): TrainingQueryBuilder
    {
        return new TrainingQueryBuilder($query);
    }

    /************
     * Relations
     ************/

    public function trainingType(): BelongsTo
    {
        return $this->belongsTo(TrainingType::class, 'training_type_id', 'id');
    }

    public function sport(): BelongsTo
    {
        return $this->belongsTo(Sport::class, 'sport_id', 'id');
    }

    public function trainer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'trainer_id', 'id');
    }

    public function onlinePlatform(): BelongsTo
    {
        return $this->belongsTo(OnlinePlatform::class, 'online_platform_id', 'id');
    }

    public function students(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'user_training',
            'training_id',
            'user_id'
        );
    }

    /************
     * Attributes
     ************/

    public function isGroupType(): bool
    {
        $this->loadMissing('trainingType');

        return $this->trainingType->slug === TrainingType::GROUP;
    }

    public function isPersonalType(): bool
    {
        $this->loadMissing('trainingType');

        return $this->trainingType->slug === TrainingType::PERSONAL;
    }
}
