<?php

namespace App\Models;

use App\QueryBuilders\SportQueryBuilder\SportQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Sport
 *
 * @property int $id
 * @property string $name
 * @property User[]|Collection $users
 * @property Training[]|Collection $trainings
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read int|null $trainings_count
 * @property-read int|null $users_count
 *
 * @method static SportQueryBuilder|Sport filterByUser(int $userId)
 * @method static SportQueryBuilder|Sport newModelQuery()
 * @method static SportQueryBuilder|Sport newQuery()
 * @method static SportQueryBuilder|Sport query()
 * @method static SportQueryBuilder|Sport setOrder(array $sort = [])
 * @method static SportQueryBuilder|Sport setPagination(array $meta = [])
 * @method static SportQueryBuilder|Sport whereCreatedAt($value)
 * @method static SportQueryBuilder|Sport whereId($value)
 * @method static SportQueryBuilder|Sport whereName($value)
 * @method static SportQueryBuilder|Sport whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Sport extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public const SORTABLE_COLUMNS = [
        'created_at',
        'name',
        'users_count',
        'trainings_count',
    ];

    public function newEloquentBuilder($query): SportQueryBuilder
    {
        return new SportQueryBuilder($query);
    }

    /************
     * Relations
     ************/

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_sport');
    }

    public function trainings(): HasMany
    {
        return $this->hasMany(Training::class, 'sport_id', 'id');
    }
}
