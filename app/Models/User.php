<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\QueryBuilders\UserQueryBuilder\UserQueryBuilder;
use App\Traits\RolesAttributes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $email
 * @property string $telegram_username
 * @property int $telegram_token
 * @property string $password
 * @property string $email_verified_at
 * @property string $created_at
 * @property UserProfile $profile
 * @property Sport[]|Collection $sports
 * @property Training[]|Collection $trainings
 * @property OnlinePlatform[]|Collection $onlinePlatforms
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $two_factor_confirmed_at
 * @property string|null $remember_token
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property Carbon|null $updated_at
 * @property-read DatabaseNotificationCollection<int, DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read int|null $online_platforms_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Permission> $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Role> $roles
 * @property-read int|null $roles_count
 * @property-read int|null $sports_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Training> $studentsTrainings
 * @property-read int|null $students_trainings_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @property-read int|null $trainings_count
 *
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static UserQueryBuilder|User filterByGender(?string $gender)
 * @method static UserQueryBuilder|User filterByRole(?int $roleId)
 * @method static UserQueryBuilder|User filterBySearchString(?string $search)
 * @method static UserQueryBuilder|User joinProfile()
 * @method static UserQueryBuilder|User joinRoles()
 * @method static UserQueryBuilder|User newModelQuery()
 * @method static UserQueryBuilder|User newQuery()
 * @method static UserQueryBuilder|User permission($permissions)
 * @method static UserQueryBuilder|User query()
 * @method static UserQueryBuilder|User role($roles, $guard = null)
 * @method static UserQueryBuilder|User selectForIndex()
 * @method static UserQueryBuilder|User selectForSelect()
 * @method static UserQueryBuilder|User setOrder(array $sort = [])
 * @method static UserQueryBuilder|User setPagination(array $meta = [])
 * @method static UserQueryBuilder|User whereCreatedAt($value)
 * @method static UserQueryBuilder|User whereCurrentTeamId($value)
 * @method static UserQueryBuilder|User whereEmail($value)
 * @method static UserQueryBuilder|User whereEmailVerifiedAt($value)
 * @method static UserQueryBuilder|User whereId($value)
 * @method static UserQueryBuilder|User wherePassword($value)
 * @method static UserQueryBuilder|User whereProfilePhotoPath($value)
 * @method static UserQueryBuilder|User whereRememberToken($value)
 * @method static UserQueryBuilder|User whereTwoFactorConfirmedAt($value)
 * @method static UserQueryBuilder|User whereTwoFactorRecoveryCodes($value)
 * @method static UserQueryBuilder|User whereTwoFactorSecret($value)
 * @method static UserQueryBuilder|User whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use HasRoles;
    use RolesAttributes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'telegram_username',
        'telegram_token',
        'password',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:d.m.Y',
    ];

    public const ROLE_ADMIN = 'admin';

    public const ROLE_TRAINER = 'trainer';

    public const ROLE_STUDENT = 'student';

    public const SORTABLE_COLUMNS = [
        'first_name',
        'last_name',
        'created_at',
        'email',
        'gender',
        'role',
        'country',
    ];

    public function newEloquentBuilder($query): UserQueryBuilder
    {
        return new UserQueryBuilder($query);
    }

    /************
     * Relations
     ************/

    public function profile(): HasOne
    {
        return $this->hasOne(UserProfile::class);
    }

    public function sports(): BelongsToMany
    {
        return $this->belongsToMany(Sport::class, 'user_sport');
    }

    public function trainings(): HasMany
    {
        return $this->hasMany(Training::class, 'trainer_id', 'id');
    }

    public function onlinePlatforms(): BelongsToMany
    {
        return $this->belongsToMany(
            OnlinePlatform::class,
            'user_online_platform',
            'user_id',
            'online_platform_id'
        )
            ->withPivot('account');
    }

    public function studentsTrainings(): BelongsToMany
    {
        return $this->belongsToMany(
            Training::class,
            'user_training',
            'user_id',
            'training_id'
        );
    }

    /************
     * Attributes
     ************/

    protected static function password(): Attribute
    {
        return Attribute::make(
            set: fn ($password) => Hash::make($password)
        );
    }
}
