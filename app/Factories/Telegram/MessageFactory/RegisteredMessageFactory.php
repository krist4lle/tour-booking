<?php

namespace App\Factories\Telegram\MessageFactory;

class RegisteredMessageFactory extends AbstractMessageFactory
{
    private array $markup = [
        [
            [
                'text' => 'Profile',
                'callback_data' => 'profile',
            ],
            [
                'text' => 'Sports',
                'callback_data' => 'sports',
            ],
            [
                'text' => 'Trainings',
                'callback_data' => 'trainings',
            ],
        ],
    ];

    public function sendMessage(int $chatId): void
    {
        $keyboard = $this->createInlineKeyboardMarkup($this->markup);
        $message = 'You have been registered. Now you can use menu to navigate in our app.';

        $this->bot->sendMessage(chatId: $chatId, text: $message, replyMarkup: $keyboard);
    }
}
