<?php

namespace App\Factories\Telegram\MessageFactory;

interface MessageFactoryContract
{
    public function sendMessage(int $chatId): void;
}
