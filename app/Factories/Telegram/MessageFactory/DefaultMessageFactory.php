<?php

namespace App\Factories\Telegram\MessageFactory;

class DefaultMessageFactory extends AbstractMessageFactory
{
    public function sendMessage(int $chatId): void
    {
        $message = 'Please start our Bot. By press or manually type /start.';

        $this->bot->sendMessage(chatId: $chatId, text: $message);
    }
}
