<?php

namespace App\Factories\Telegram\MessageFactory;

use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;

abstract class AbstractMessageFactory implements MessageFactoryContract
{
    protected BotApi $bot;

    public function __construct()
    {
        $this->bot = new BotApi(config('telegram.token'));
    }

    protected function createReplyKeyboardMarkup(array $markup, bool $isOneTime = true): ReplyKeyboardMarkup
    {
        return new ReplyKeyboardMarkup($markup, $isOneTime);
    }
}
