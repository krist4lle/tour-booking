<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class IsSameTrainer implements ValidationRule
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->user->isTrainer() && $value !== $this->user->id) {
            $message = sprintf(
                'Trainer %s can not add a Training to another Trainer. The :attribute must be %s.',
                $this->user->profile->fullName,
                $this->user->id
            );

            $fail($message);
        }
    }
}
