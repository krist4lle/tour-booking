<?php

namespace App\Rules;

use App\Services\RolePermission\RolePermissionServiceContract;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class InActionsList implements ValidationRule
{
    private RolePermissionServiceContract $rolePermissionService;

    public function __construct()
    {
        $this->rolePermissionService = app(RolePermissionServiceContract::class);
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $actions = $this->rolePermissionService->getActions();

        if (! in_array($value, $actions)) {
            $fail('The :attribute must be in list: '.implode(', ', $actions));
        }
    }
}
