<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class TrainerHasSport implements ValidationRule
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $sportsIds = $this->user->sports->pluck('id')->toArray();

        if ($this->user->isTrainer() && ! in_array($value, $sportsIds)) {
            $message = sprintf(
                'Trainer %s can not create a Training with Sport which he/she does not have. The :attribute must be in list %s.',
                $this->user->profile->fullName,
                implode(', ', $sportsIds)
            );

            $fail($message);
        }
    }
}
