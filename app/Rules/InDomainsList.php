<?php

namespace App\Rules;

use App\DTO\Domain\DomainDTO;
use App\Services\RolePermission\RolePermissionServiceContract;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class InDomainsList implements ValidationRule
{
    private RolePermissionServiceContract $rolePermissionService;

    public function __construct()
    {
        $this->rolePermissionService = app(RolePermissionServiceContract::class);
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $domains = $this->rolePermissionService->getDomains();
        $listOfDomains = $domains->implode(fn (DomainDTO $domainDTO) => $domainDTO->getClass(), ', ');

        if (! $domains->contains(fn (DomainDTO $domainDTO) => $domainDTO->getClass() === $value)) {
            $fail('The :attribute must be in list: '.$listOfDomains);
        }
    }
}
