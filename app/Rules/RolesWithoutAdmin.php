<?php

namespace App\Rules;

use App\Repositories\Role\RoleRepository;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;
use Spatie\Permission\Models\Role;

class RolesWithoutAdmin implements ValidationRule
{
    private RoleRepository $roleRepository;

    public function __construct()
    {
        $this->roleRepository = new RoleRepository(Role::class);
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $rolesWithoutAdminIds = $this->roleRepository->getWithoutAdmin()->pluck('id')->toArray();

        if (! in_array($value, $rolesWithoutAdminIds)) {
            $fail('You can not register as Admin.');
        }
    }
}
