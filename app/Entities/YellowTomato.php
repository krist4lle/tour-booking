<?php

namespace App\Entities;

class YellowTomato extends Tomato
{
    protected static string $color = 'yellow';
}
