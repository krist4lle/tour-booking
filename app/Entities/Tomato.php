<?php

namespace App\Entities;

class Tomato
{
    protected static string $shape = 'round';

    protected static string $color = 'red';

    public static function getShape(): string
    {
        return static::$shape;
    }

    public static function getColor(): string
    {
        return static::$color;
    }
}
