1) composer install

2) npm install

3) npm run build

4) cp .env.example .env

5) set .env

   DB_CONNECTION=mysql
   DB_HOST=mysql
   DB_PORT=3306
   DB_DATABASE=tour_booking
   DB_USERNAME=sail
   DB_PASSWORD=password

6) ./vendor/bin/sail up -d

7) ./vendor/bin/sail artisan optimize

8) ./vendor/bin/sail artisan migrate --seed

9) ./vendor/bin/sail artisan app:create-super-admin

10) use
