<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DomainController;
use App\Http\Controllers\OnlinePlatformController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SportController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\TrainingTypeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/telegram/webhook', [TelegramController::class, 'handleWebhook']);

Route::prefix('/roles')->group(function () {
    Route::get('/', [RoleController::class, 'index']);
});

Route::prefix('/users')->group(function () {
    Route::get('/current', [UserController::class, 'current']);
    Route::get('/set-locale', [UserController::class, 'setLocale']);
});

Route::group(['middleware' => 'guest'], function () {
    Route::prefix('/auth')->group(function () {
        Route::post('/register', [AuthController::class, 'register']);
        Route::post('/login', [AuthController::class, 'login']);
    });
});

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/auth')->group(function () {
        Route::post('/logout', [AuthController::class, 'logout']);
    });

    Route::prefix('/users')->controller(UserController::class)->group(function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::get('/{user}', 'show');
        Route::put('/{user}', 'update');
        Route::delete('/{user}', 'delete');
        Route::post('/update-password', 'updatePassword');
    });

    Route::prefix('/sports')->controller(SportController::class)->group(function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::put('/{sport}', 'update');
        Route::delete('/{sport}', 'delete');
        Route::post('/attach', 'attach');
        Route::post('/detach', 'detach');
    });

    Route::prefix('/roles')->controller(RoleController::class)->group(function () {
        Route::get('/{role}', 'show');
        Route::post('/sync', 'sync');
    });

    Route::prefix('/permissions')->controller(PermissionController::class)->group(function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::put('/{permission}', 'update');
        Route::delete('/{permission}', 'delete');
    });

    Route::prefix('/domains')->controller(DomainController::class)->group(function () {
        Route::get('/', 'index');
    });

    Route::prefix('/online-platforms')->controller(OnlinePlatformController::class)->group(function () {
        Route::get('/', 'index');
        Route::post('/', 'store');
        Route::put('/{onlinePlatform}', 'update');
        Route::delete('/{onlinePlatform}', 'delete');
        Route::post('/attach', 'attach');
        Route::post('/detach', 'detach');
    });

    Route::prefix('/trainings')->controller(TrainingController::class)->group(function () {
        Route::get('/', 'index');
        Route::get('/{training}', 'show');
        Route::post('/', 'store');
        Route::put('/{training}', 'update');
        Route::delete('/{training}', 'delete');
        Route::post('/attach', 'attach');
        Route::post('/detach', 'detach');
    });

    Route::prefix('/training-types')->group(function () {
        Route::get('/', [TrainingTypeController::class, 'index']);
    });
});
